/******************************************************************
 * Site accknowlegement: stackoverflow.com
 *                          => sleep_for() was from user bames53
 *
 *                       cplusplus.com
 *                       w3school.com
 *                       geeksforgeeks.org
 *                       codeproject.com
 *                       techiedelight.com
 *
 *                       slack cpp channels
 *                       (user: Vinnie Falco)
 *                       not understanding how to use his
 *                       boost/json.h made me write my own
 *
 *                       youtube: Hathibelagal Productions
 *                       I liked his simple libjsonc program
 *                       but I felt I could make mine simpiler.
 *                       which it's probably not :-D
 *****************************************************************/

#include <chrono>         // nanoseconds, seconds, system_clock, etc
#include <thread>         // sleep_for, sleep_until
#include <cstdio>         // sprintf(), sscanf()
#include <iostream>       // cout, etc
#include <fstream>        // File I/O
#include <sstream>        // String functions
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <iterator>
#include <array>

using namespace std;
using namespace this_thread;
using namespace chrono;

class jsonParse
{
	private:

/***************************************************************
 * vector (jsDataMod) will eventually look like this
 *
 * ============================================
 * | key ("age")  |  data ("30") |   element  |
 * ============================================
 ***************************************************************/

       vector< vector<string> > jsDataMod;
       vector< string > jsDataRaw;
       vector<string> keys;
       vector<string> keyData;
       vector<string> KeyBrkt;
       vector<vector <string>> noKeyAry;
       vector<vector <string>> noKeyAryName;
       vector<vector <string>> assoKeyAry;
       vector<string> dta;
	   vector<string> qList;
	   vector<string> qList2;
	   vector<vector <string>> AccessData;
	   vector<vector <string>> txtFileVar;
	   vector<vector <string>> SectModData;
	   vector<string> SearchResults;
	   vector<string> mathStr;
	   vector<string> EndComma;

       vector<int> noKeyElm;
       vector<int> noKeyAryCnt;
       vector<int> assoKeyElm;
       vector<int> spaceCnt;
       vector<int> bracketID;
       vector<vector <unsigned int>> colonsCnt;
       vector<int> varPreload1;
       vector <int> openPar;
       vector <int> closePar;

       int col;
	   int qty1;
	   int add1;
	   int sub1;
       char ConvNum [1024];
	   int row;
	   int cnt = 0;
	   int cnt1;
       int cnt2;
       int cnt3;
       int cnt4;
       int cnt5;
       int cnt6;
       int cnt7;
	   int addCnt;
       int subCnt;
       int mulCnt;
       int divCnt;
       int ogEndCnt;
       int modEndCnt;
       int elm1;
       int elmCK1;
       int elmCK2;
       int elmCK3;
       int elmCK4;
       int mathInt1;
       int mathInt2;
       int mathInt3;
       int mathInt4;
       int mathInt5;
       int mathInt6;
       int mathInt7;
       int mathInt8;
       int temp1 = 45;
       int oMovies = 3;
       int nMovies = 5;
       int cBars = 3;
       int gummies = 3;
       int bCandy = 4;
       int popCorn = 4;

       string buf1;
       string line;
	   string line2;
       string line3;
       string tmp1;
       string tmp2;
       string tmp3;
       string tmp4;
       string strEnding;
       string myString;
       string FileName;
       string FileBKup;
       string FileUserFiles;
       string FileVariables;
       //string **temp1;
       fstream myfile;
       string passData;
       string command;

  	   bool BracketFound;
       bool hasDot;
       bool varExist;
       bool UpdatAssoAry;

    void appendFileName()
    {
        int lCnt = 0;

        string strFile;
        string lAns;

        bool ck1 = false;

        ReadFile(4);
        lCnt = jsDataRaw.size();

        if(lCnt > 0){

            if(txtFileVar.size() > 0){ txtFileVar.clear(); }
            if(dta.size() > 0){ dta.clear(); }
            dta.resize(2, " ");

            for(int i=0; i<lCnt; i++){

               dta[0] = jsDataRaw[i];
               dta[1] = jsDataRaw[i+1];
               txtFileVar.push_back(dta);
               ++i;
            }
        }

        while(!ck1){
           titleScreen(6);
           cout<<"Enter here => ";
           getline(cin, strFile);

           cout<<"You entered [ "<<strFile<<" ]"<<endl;
           cout<<"Is that correct?\n";
           cout<<"Enter [y] or [n] => ";

           getline(cin, lAns);
           if((lAns == "y") || (lAns == "Y")){
               ck1 = true;
           }
        }

        WriteUserFile(strFile);
        titleScreen(1);
    }

	void assoKeyCheck()
	{
      /*************
       * Key Array *
       *************
       finding the keys by using the colonsCnt
       array as a stop count in the for loop */
	   for(int i=0; i<row; i++){
         line = jsDataMod[i][0];
         line2 = "";
         cnt = colonsCnt[i][0];

      // storing the key
         for(int j=0; j<cnt; j++){
            line2 += line[j];
            keys[i] = line2;
            jsDataMod[i][0] = line2;
		 }

	  /* removing the "," from the end of
		 each line in the array */
		 cnt1 = line.size() - 1;
		 cnt2 = 0;
		 tmp1 = "";
		 tmp2 = line[cnt1];

	  /* if the comma is there, (max cnt - 1)
		 Or, comma isn't there so keep the
		 origninal line cnt */

		 if(tmp2 == ","){ cnt2 = cnt1; }
		 else{ cnt2 = line.size(); }

		 for(int k=cnt+1; k<cnt2; k++){ tmp1 += line[k]; }

         line2 = tmp1[0];

      // removing the beginning " " from the string
         if(line2 == " "){

		    line2 = tmp1;
		    tmp1 = "";

		    for(unsigned l=1; l<line2.size(); l++){
			   tmp1 += line2[l];
		    }
		 }

	  // this is the key's data
		 keyData[i] = tmp1;

		 tmp3 = jsDataMod[i][1];
		 if(tmp3 != " "){
			jsDataMod[i][1] = tmp1;
		 }

		 line = tmp1;
		 tmp1 = line[1];
	   }
	}

	bool Asso1Var(vector <string> &localData, vector <int> &localElm,
                        bool &b4Var, bool &afVar, int &b4Int, int &afInt)
	{
        bool ck = false;

        int lCnt, lCnt2, lElm, lElm2;
        lCnt = lCnt2 = lElm = lElm2 = 0;
        string str1, str2;
        str1 = str2 = "";

     /* Checking to see if the user entered
        a key word that has only one occurance
        or multiple occurances */

        lCnt = localData.size();
        if(lCnt == 1){

           str1 = localData[0];
           lCnt = keys.size();

           for(int i=0; i<lCnt; i++){
               str2 = keys[i];
               if(str1 == str2){
                  lCnt2++;
                  lElm = i;
               }
           }

           if(lCnt2 < 2){

             lCnt = localElm.size();
             for(int i=0; i<lCnt; i++){
               if(lElm == localElm[i]){
                  lElm2 = i;
               }
             }
           }


           if(b4Var){

              if(!afVar){

               /* only one occurance is found so
                  show the elm data */
                  if(lCnt2 < 2){
                     b4Int++;
                     elmCK1 = lElm2;
                     elmCK2 = b4Int;

                  // store keydata in public variable
                     vStr = assoKeyAry[elmCK1][elmCK2];
                     cout<<"Current value [ "<<vStr<<" ]"<<endl;
                     setDataPosition("assoAry", lElm);
                  }
                  else{

                  /* multiple occurances are found but
                     they only entered one bracket.
                     So show the entire list */
                     vStr = keyData[localElm[b4Int]];
                     cout<<"Current list "<<vStr<<endl;
                  }
              }
              else{

               /* user typed in both brackets
                  so show the elm data */
                  afInt++;
                  lElm = localElm[b4Int];
                  elmCK1 = b4Int;
                  elmCK2 = afInt;

               // store keydata in public variable
                  vStr = assoKeyAry[elmCK1][elmCK2];
                  cout<<"Current value [ "<<vStr<<" ]"<<endl;
                  setDataPosition("assoAry", lElm);
              }
              ck = true;
           }
        }

        return ck;
	}

	bool Asso2Var(vector <string> &localData, vector <int> &localElm,
                             bool &b4Var, int &b4Int, string &strBrkt)
	{
	    bool ck = false;
	    bool ck2 = false;
        int lCnt, lCnt2, lCnt3, lElm, lElm2, lElm3;
        lCnt = lCnt2 = lCnt3 = lElm = lElm2 = lElm3 = 0;
        string str1, str2, str3;
        str1 = str2 = str3 = "";

        vector<string> localStr;
        localStr.resize(2, "\"");

        lCnt = localData.size();

        if(bracketID.size() > 0){

           if(b4Var){
              lCnt = bracketID[0];
              lElm = localElm[lCnt];

              str1 = strBrkt[0];
              if(str1 == "\""){
                 strBrkt.erase(0, 1);
                 strBrkt.erase(strBrkt.size()-1, 1);
              }

              str1 = strBrkt[0];

              if(str1 == "["){
                 strBrkt.erase(0, 1);
                 strBrkt.erase(strBrkt.size()-1, 1);
              }

	       // converting strBrkt to integer
		      istringstream (strBrkt) >> lElm2;

              elmCK1 = lCnt;
              elmCK2 = lElm2+=2;
              vStr = assoKeyAry[elmCK1][elmCK2];

           // store keydata in public variable
              cout<<"Current value [ "<<vStr<<" ]"<<endl;
              setDataPosition("assoAry", lElm);
              ck = true;
           }
       }
       else if((lCnt == 2) && (b4Var)){
           str1 = localData[0];
           lCnt2 = keyData.size();

           for(int i=0; i<lCnt2; i++){

              str2 = keyData[i];
              if(str1 == str2){
                 lElm = i+1;
                 ck2 = true; break;
              }
           }

           if(ck2){

              str1 = localData[1];
              lCnt2 = keys.size();

              for(int i=lElm; i<lCnt2; i++){

                 str2 = keys[i];
                 if(str1 == str2){

                   lElm = i;
                   lCnt3 = localElm.size();

                   for(int j=0; j<lCnt3; j++){
                      if(lElm == localElm[j]){
                         lElm3 = j;
                         break;
                      }
                   }
                   break;
                 }
              }

              str1 = strBrkt[0];
              str3 = strBrkt;
              str2 = strBrkt;

              if(str1 == "\""){
                 str3.erase(0, 1);
                 str3.erase(str3.size()-1, 1);

                 str1 = strBrkt[0];

                 if(str1 == "["){
                    str3.erase(0, 1);
                    str3.erase(str3.size()-1, 1);

	             // converting strBrkt to integer
		            istringstream (str3) >> lElm2;

		            sprintf(ConvNum, "%d", lElm2);
		            localStr[1] = ConvNum;
		            str3 = localStr[1];

		            if(str1 != str3){

		           // it's not a number so make it a string again
		              localStr[1] = localStr[0] + strBrkt + localStr[0];
		            }
                    else{
                    /* it is a number because it passed the str3
                       check test */
                       strBrkt.erase(0, 1);
                       strBrkt.erase(strBrkt.size()-1, 1);
                       strBrkt.erase(0, 1);
                       strBrkt.erase(strBrkt.size()-1, 1);
                    }

                 }
                 else{
                    lElm2 = b4Int;
                 }
              }
              else if(str2 == ""){
                lElm2 = b4Int;
              }

              elmCK1 = lElm3;
              elmCK2 = lElm2 += 1;
              vStr = assoKeyAry[elmCK1][elmCK2];

           // store keydata in public variable
              cout<<"Current value [ "<<vStr<<" ]"<<endl;
              setDataPosition("assoAry", lElm);
              ck = true;
           }
       }

	   return ck;
	}

	bool Asso3Var(vector <string> &localData, vector <int> &localElm,
                             bool &b4Var, int &b4Int, string &strBrkt)
	{
	    bool ck = false;

	    int lCnt, lCnt2, lCnt3, lElm, lElm2, lElm3;
	    lCnt = lCnt2 = lCnt3 = lElm = lElm2 = lElm3 = 0;

	    string str1, str2, str3;
	    str1 = str2 = str3 = "";

        lCnt = localData.size();

        for(unsigned int i=0; i<qList2.size(); i++){
           str1 = qList2[i];
           str2 = str1[0];

           if(str2 == "["){
              b4Var = true;
              break;
           }
        }

        if(bracketID.size() > 0){

           if((lCnt == 3) && (b4Var)){
              lCnt = bracketID[0];
              lElm = localElm[lCnt];

              str1 = strBrkt[0];
              if(str1 == "\""){
                 strBrkt.erase(0, 1);
                 strBrkt.erase(strBrkt.size()-1, 1);
              }

              str1 = strBrkt[0];

              if(str1 == "["){
                 strBrkt.erase(0, 1);
                 strBrkt.erase(strBrkt.size()-1, 1);
              }

	       // converting strBrkt to integer
		      istringstream (strBrkt) >> lElm2;

              elmCK1 = lCnt;
              elmCK2 = lElm2+1;
              vStr = assoKeyAry[elmCK1][elmCK2];

           // store keydata in public variable
              cout<<"Current value [ "<<vStr<<" ]"<<endl;
              setDataPosition("assoAry", lElm);
              ck = true;
           }
       }
       else if((lCnt == 3) && (b4Var)){

           str1 = localData[0];
           lCnt2 = keys.size();

           for(int i=0; i<lCnt2; i++){

              str2 = keys[i];
              if(str1 == str2){
                 lElm = i+1;
                 break;
              }
           }

           str1 = localData[1];
           for(int i=lElm; i<lCnt2; i++){

              str2 = keys[i];
              if(str1 == str2){
                 lElm = i+1;
                 break;
              }
           }

           str1 = localData[2];
           for(int i=lElm; i<lCnt2; i++){

              str2 = keys[i];
              if(str1 == str2){
                 lElm = i;
                 break;
              }
           }

           lCnt2 = localElm.size();
           for(int i=0; i<lCnt2; i++){
              if(lElm == localElm[i]){
                 lElm3 = i;
              }
           }

           str1 = strBrkt[0];
           if(str1 == "\""){
              strBrkt.erase(0, 1);
              strBrkt.erase(strBrkt.size()-1, 1);
           }

           str1 = strBrkt[0];

           if(str1 == "["){
              strBrkt.erase(0, 1);
              strBrkt.erase(strBrkt.size()-1, 1);
           }

	    // converting strBrkt to integer
		   istringstream (strBrkt) >> lElm2;

           elmCK1 = lElm3;
           elmCK2 = lElm2 += 1;
           vStr = assoKeyAry[elmCK1][elmCK2];

        // store keydata in public variable
           cout<<"Current value [ "<<vStr<<" ]"<<endl;
           setDataPosition("assoAry", lElm);
           ck = true;
       }

	   return ck;
	}

	void BeforeWriteCheck()
	{
	    string str1 = "";
	    string sJSD, sKeys, sKeyD, sBrkt, sComma, sResult;
	    sJSD = sKeys = sKeyD = sBrkt = sComma = sResult = "";

        int lRow = jsDataMod.size();

        for(int i=0; i<lRow; i++){

           sJSD = jsDataMod[i][0];
           sKeys = keys[i];
           sKeyD = keyData[i];
           sBrkt = KeyBrkt[i];
           sComma = EndComma[i];

           if(i == 0){
              jsDataMod[i][0] = KeyBrkt[i];
           }
           else{

               if(sKeys != ""){

                  sResult += sKeys + ": ";
                  if(sBrkt == sKeyD){
                     sResult += sBrkt;
                     jsDataMod[i][0] = sResult;
                  }
                  else{

                     sResult += sKeyD;
                     str1 = sResult[sResult.size()-1];

                     if(str1 != ","){

                        if(str1 == " "){
                           sResult += sBrkt;
                        }
                        sResult += sComma;
                     }

                  /* removing commas and any
                     other unwanted data */
                     else{
                        sResult.erase(sResult.size()-1, 1);
                        sResult += sComma;
                     }

                     jsDataMod[i][0] = sResult;
                  }
               }

/*
            // Single { or } or }, or ], etc
               else if(sJSD == sBrkt){
                  cout<<jsDataMod[i][0]<<endl;
               }
*/
            // noKeyAry data
               else if((sKeys == "") && (sBrkt == "") &&
                       ((sKeyD == " ") || (sKeyD == ""))){

                  sResult = jsDataMod[i][0];
                  str1 = sResult[sResult.size()-1];

                  if(str1 != ","){
                     sResult += sComma;
                     jsDataMod[i][0] = sResult;
                     //cout<<jsDataMod[i][0]<<endl;
                  }
                  else{
                     sResult.erase(sResult.size()-1, 1);
                     sResult += sComma;
                     jsDataMod[i][0] = sResult;
                     //cout<<jsDataMod[i][0]<<endl;
                  }
               }/* end else if((sKeys == "") && (sBrkt == "") &&
                              ((sKeyD == " ") || (sKeyD == ""))) */
           }//     end else

           sResult = "";

        }// for(int i=0; i<lRow; i++)
	}

	void change1(bool hasDot)
	{
		int lElm;
		int lDataCnt;
		string str1, str2, str3;
		bool ck = false;

        lDataCnt = AccessData.size();
        if(lDataCnt > 0){

	   // "keyData" or "jsDataMod" or "assoAry"
		   str1 = AccessData[0][0];

	    // Element
		   str2 = AccessData[0][1];

	    // Data
		   str3 = AccessData[0][2];

	    // converting strElm to integer
		   istringstream (str2) >> lElm;
		   convPubInt_2_StrVal();

		   if(str1 == "keyData"){

			   if(str3 != ""){ vStr = str3; }

			   str3 = "";

			   for(unsigned int i=0; i<vStr.size(); i++){
				  str3 += vStr[i];
			   }

               updateAssoAry();
               updateNoKeyData();
               BeforeWriteCheck();
               WriteBkup(1);
			   keyData[lElm] = str3;
			   cout<<"changed to "<<keyData[lElm]<<endl;
			   ck = true;
		   }
		   else if(str1 == "jsDataMod"){
		       BeforeWriteCheck();
		       WriteBkup(1);
               noKeyAry[elmCK1][elmCK2] = str3;
               updateAssoAry();
               updateNoKeyData();
               cout<<"changed to "<<jsDataMod[lElm][0]<<endl;
               ck = true;
		   }
		   else if(str1 == "assoAry"){
		       BeforeWriteCheck();
		       WriteBkup(1);
		       assoKeyAry[elmCK1][elmCK2] = str3;
		       updateAssoAry();
		       updateNoKeyData();
		       cout<<"changed to [ "<<assoKeyAry[elmCK1][elmCK2]<<" ]"<<endl;
		   }

	    }

	    if(!ck){ updateNoKeyData(); }

	    BeforeWriteCheck();
	    WriteFile();
	}

	bool changeTopLevel(bool hasDot)
	{

		string str1, str2, str3;
		str1 = str2 = str3 = "";
		int lElm = 0;
		bool ck = false;

		cnt = qList.size();
		cout<<"changeTopLevel cnt = "<<cnt<<endl;

		findTopLevel(hasDot);


		if(hasDot){

           str1 = AccessData[0][0];
           str2 = AccessData[0][1];

           cout<<"\nstr1 = "<<str1<<endl;

           cout<<"passData = "<<passData<<endl;

           if(str1 == "keyData"){

		    // Converting the string to integer
	           istringstream (str2) >> lElm;
;
			    cout<<"current value "<<keyData[lElm]<<endl;

			    keyData[lElm] = passData;
			    cout<<"changed to "<<keyData[lElm]<<endl;
			    ck = true;
		   }
		 }
		 else{

             str1 = AccessData[0][0];
             str2 = AccessData[0][1];

             if(str1 == "keyData"){

		      // Converting the string to integer
	             istringstream (str2) >> lElm;

		         //scanf(str2.c_str(), "%d", &lElm);
			     cout<<"current value "<<keyData[lElm]<<endl;

			     keyData[lElm] = passData;
			     cout<<"changed to "<<keyData[lElm]<<endl;
			     ck = true;
		     }
		 }

		return ck;
	}

	void clearArrays()
	{
	// (clean-up) clearing all vectors
	   if(jsDataRaw.size() > 0){ jsDataRaw.clear(); }

	   if(jsDataMod.size() > 0){ jsDataMod.clear(); }

	   if(keys.size() > 0){ keys.clear(); }

	   if(noKeyAry.size() > 0){ noKeyAry.clear(); }

	   if(assoKeyAry.size() > 0){ assoKeyAry.clear(); }

	   if(dta.size() > 0){ dta.clear(); }

	   if(noKeyElm.size() > 0){ noKeyElm.clear(); }

	   if(assoKeyElm.size() > 0){ assoKeyElm.clear(); }

	   if(spaceCnt.size() > 0){ spaceCnt.clear(); }

	   if(colonsCnt.size() > 0){ colonsCnt.clear(); }

	   if(AccessData.size() > 0){ AccessData.clear(); }

	   if(qList.size() > 0){ qList.clear(); }

	   if(qList2.size() > 0){ qList2.clear(); }

	   if(bracketID.size() > 0){ bracketID.clear(); }

	   if(mathStr.size() > 0){ mathStr.clear(); }

	   if(varPreload1.size() > 0){ varPreload1.clear(); }

	   if(openPar.size() > 0){ openPar.clear(); }

	   if(closePar.size() > 0){ closePar.clear(); }

	   if(EndComma.size() > 0){ EndComma.clear(); }

	   if(KeyBrkt.size() > 0){ KeyBrkt.clear(); }

	   if(noKeyAryName.size() > 0){ noKeyAryName.clear(); }


/*
	    // deallocate (free) the memory
           for (int i = 0; i < row; i++ )
           {
	          delete [] temp1[i];
           }
           delete temp1;
*/
	}

	void clearScreen()
	{
	   titleScreen(1);
	}

	void CloseFile()
	{
	    myfile.close();
	}

	void convPubIntVal(){

		bool fndStr = false;
		string str1, str2;
		str1 = str2 = "";

		int lCnt = vStr.size();

	 /* Determing if the data found
        is a str or int */
        for(int i=0; i<lCnt; i++){
 		   str1 = vStr[i];

		   if(str1 != "\""){
		     str2 += str1;
		   }
		   else{
			 vInt = 0;
			 fndStr = true;
			 break;
		   }
		}

     // data is an int so store it public variable
		if(!fndStr){

	       istringstream (str2) >> vInt;
		   //sscanf(str2.c_str(), "%d", &vInt);
	    }
	}

	void convPubInt_2_StrVal()
	{
		sprintf(ConvNum, "%d", vInt);
		AccessData[0][2] = ConvNum;
	}

	void find(bool hasDot)
	{
		bool ck = false;

        if(hasDot){

        // Checking for top level search
		   if(!ck){
		     ck = findTopLevel(hasDot);
		   }

        // Checking for key arrays
		   if(!ck){
		      ck = findAssoKey();
		   }

	    /* This search is performed when
	       the user searches for two key words
	       ( "Currency" "qty" or "playlist" "artist") */

		   if(!ck){
			  ck = find2Keys();
		   }

	    /* This search is performed when
	       the user searches for three key words
	       ( "Company" "BrokerID" "client" or "stock") */

		   if(!ck){
			  ck = find3Keys();
		   }

		}
		else{

		  if(!ck){
		     ck = findAssoKey();
		  }

       // Checking for top level search
          if(!ck){
			 ck = findTopLevel(hasDot);
		     //cout<<"FindTopLevel ck = "<<ck<<endl;
		  }


	   /* This search is performed when
	      the user searches for a keyData word
	      and then a key ( "Alice" ("age" or "friends")) */

		  if(!ck){
			 ck = findKeyData_Key();
		     //cout<<"FindKeysData_Key ck = "<<ck<<endl;
		  }


	   /* This search is performed when
	      the user searches for two key words
	      ( "Currency" "qty" or "playlist" "artist") */

		  if(!ck){
			 ck = find2Keys();
			 //cout<<"Find2Keys ck = "<<ck<<endl;
		  }


	   /* This search is performed when
	      the user searches for three key words
	      ( "Company" "BrokerID" "client" or "stock") */

		  if(!ck){
			  ck = find3Keys();
			  //cout<<"Find3Keys ck = "<<ck<<endl;
		  }


	   /* This search is performed when the user
	      searches for a key word and then a nonkey
	      word ( "shoes" "Jordan" or "AirMax) */

          if(!ck){
             ck = findKey_NonKey();
             //cout<<"FindKey_NonKey ck = "<<ck<<endl;
	      }
	    }
	}

	bool findAssoKey()
	{
		bool ck = false;
		bool b4Var = false;
		bool afVar = false;

		string str1 = "";
		string str2 = "";
		vector<string> localData;
		vector<string> localStr;

		int lCnt = 0;
		int lCnt2 = 0;
		int b4Int = 0;
		int afInt = 0;
		vector <int> localElm;

		cnt2 = qList.size();
        localData.resize(cnt2, " ");
        localStr.resize(2, "\"");

     // copying original data
        for(int i=0; i<cnt2; i++){
           localData[i] = qList[i];
        }

     /* seeing if keyData as brackets
        stripping away those brackets and
        converting the remaining number to int */

        for(int i=0; i<cnt2; i++){
           str1 = localData[i];
           str2 = str1[0];

           if(str2 == "["){

              str1.erase(0, 1);
              str1.erase(str1.size()-1, 1);

              if(!b4Var){
                 istringstream (str1) >> b4Int;
                 localData.erase(localData.begin() + i);
                 b4Var = true;
                 --i;
              }
              else{
                 istringstream (str1) >> afInt;
                 localData.erase(localData.begin() + i);
                 afVar = true;
              }

              lCnt = localData.size();
              if(i == lCnt-1){ break; }
              //hasBrkt = true;
           }
        }

     /* putting "" around the user typed
        keywords if they don't have them */

        for(int i=0; i<cnt2; i++){

           str1 = localData[i];
           str2 = str1[0];
           if(str2 == "\""){
              localData[i] = str1;
           }
           else{
              localStr[1] = localStr[0] + str1 + localStr[0];
              localData[i] = localStr[1];
           }
        }

     // founding out how many key arrays there are
		lCnt = keyData.size();
		for(int i=0; i<lCnt; i++){
		   str1 = keyData[i];
		   lCnt2 = str1.size();

		   str2 = str1[0];
		   if(str2 == "["){
		      localElm.push_back(i);
		   }
		}

        lCnt = localElm.size();
		for(int i=0; i<lCnt; i++){

		   str1 = keyData[localElm[i]];
		   str2 = jsDataMod[localElm[i] + 1][0];

		   if((str1 == "[") && (str2 == "{")){
		       localElm.erase(localElm.begin() + i);
		       i--;
		   }

           lCnt = localElm.size();
           if(i == lCnt-1){ break; }
		}

		if(cnt2 == 4){
		   passData = localData[cnt2-1];
		   localData.erase(localData.end() - 1);
		   cnt2 = localData.size();
		}

        if(b4Var){
		   lCnt = passData.size();
		   for(int i=0; i<lCnt; i++){
		      str1 = passData[i];

		      if(str1 == "\""){ break; }

           /* This converts the string to a number
              if they match then it's a real number.
              if not, it's a string */

		      istringstream (str1) >> lCnt2;
		      sprintf(ConvNum, "%d", lCnt2);
		      localStr[1] = ConvNum;
		      str2 = localStr[1];

		      if(str1 != str2){
		         localStr[1] = localStr[0] + passData + localStr[0];
		         passData = localStr[1];
		         break;
		      }
		   }
		}

 	    if(!ck){
		   ck = Asso1Var(localData, localElm, b4Var, afVar, b4Int, afInt);
		}

 	    if(!ck){
		   ck = Asso2Var(localData, localElm, b4Var, b4Int, passData);
		}

 	    if(!ck){
		   ck = Asso3Var(localData, localElm, b4Var, b4Int, passData);
		}

  	 // clearing old data
		if(localData.size() > 0){ localData.clear(); }
		if(localStr.size() > 0){ localStr.clear(); }
		if(localElm.size() > 0){ localElm.clear(); }

        return ck;
	}

	bool findTopLevel(bool hasDot)
	{
		bool ck = false;

        string str1, str2, str3;
		vector<string> localData;
		vector<string> localStr;
		vector<int> localElm;
		vector<int> NameElm;

		int lCnt = 0;
		int lCnt2 = 0;
		int lElm = 0;
		int lElm2 = 0;


        str1 = str2 = str3 = "";
		cnt = qList.size();

        localData.resize(cnt, " ");
        localStr.resize(2, "\"");

        for(int i=0; i<cnt; i++){
           localData[i] = qList[i];
        }

		for(int i=0; i<cnt; i++){
		   str1 = localData[i];

		   str2 = str1[0];
		   if(str2 == "["){
		      str1.erase(0, 1);

		      if(str1.size() > 0){
		         str1.erase(str1.size()-1, 1);
		         istringstream (str1) >> lElm;

		      // removing the command words from the array
			     localData.erase(localData.end() - 1);
		      }
		   }
		}

		cnt = localData.size();

     /* putting "" around the user typed
        keywords if they don't have them */

        for(int i=0; i<cnt; i++){

           str1 = localData[i];
           str2 = str1[0];
           if(str2 == "\""){
              localData[i] = str1;
           }
           else{
              localStr[1] = localStr[0] + str1 + localStr[0];
              localData[i] = localStr[1];
           }
        }

     // founding out how many key arrays there are
		lCnt = keyData.size();
		for(int i=0; i<lCnt; i++){
		   str1 = keyData[i];
		   //lCnt2 = str1.size();

		   str2 = str1[0];
		   if(str2 == "["){
		      if(keys[i] == localData[0]){
		         localElm.push_back(i);
		      }
		   }
		}

		if(cnt < 2){

		   if(localElm.size() > 0){

             // store keydata in public variable
				vStr = keyData[localElm[lElm]];
				cout<<"current value "<<vStr<<endl;

                setDataPosition("keyData", lElm);

                ck = true;

           }// end if(localElm.size() > 0)
           else{

		      for(int i=0; i<cnt; i++){

		       // user typed keyword
		          tmp1 = localData[i];

		          for(unsigned int j=0; j<keys.size(); j++){

			       // keyword from the file
				      tmp2 = keys[j];

			       // This is the top level search ("name", "age", etc)
				      if(cnt < 3){
                         if(tmp1 == tmp2){

					     // store keydata in public variable
					        vStr = keyData[j];
					        cout<<"current value "<<vStr<<endl;

                            setDataPosition("keyData", j);

					        ck = true; break;
				         }
				      }
			       }
		      }// end for(unsigned int j=0; j<keys.size(); j++)

		      if(!ck){
		         for(int i=0; i<cnt; i++){

		          // user typed keyword
		             tmp1 = localData[i];

		             for(unsigned int j=0; j<keyData.size(); j++){

			          // keyword from the file
				         tmp2 = keyData[j];

			          // This is the top level search ("name", "age", etc)
				         if(cnt < 3){
                            if(tmp1 == tmp2){

					        // store keydata in public variable
					           vStr = keyData[j];
					           cout<<"current value "<<vStr<<endl;

                               setDataPosition("keyData", j);

					           ck = true; break;
				            }
				         }
			          }

		         }// end for(unsigned int j=0; j<keys.size(); j++)
		      }//    end if(!ck)

              if(!ck){

               // finding a keyword in a assoAry
                  if(localElm.size() > 0){ localElm.clear(); }

               // founding out how many key arrays there are
		          lCnt = keyData.size();
		          for(int i=0; i<lCnt; i++){
		             str1 = keyData[i];
		             lCnt2 = str1.size();

		             str2 = str1[0];
		             if(str2 == "["){
		               localElm.push_back(i);
		             }
		          }

                  lCnt = localElm.size();
		          for(int i=0; i<lCnt; i++){

                     str1 = keyData[localElm[i]];
		             str2 = jsDataMod[localElm[i] + 1][0];

		             if((str1 == "[") && (str2 == "{")){
		               localElm.erase(localElm.begin() + i);
		               i--;
		             }

                     lCnt = localElm.size();
                     if(i == lCnt-1){ break; }
		          }

		         str1 = localData[0];
		         lCnt = assoKeyAry.size();

		         for(int i=0; i<lCnt; i++){
		             lCnt2 = assoKeyAry[i].size();

		           for(int j=0; j<lCnt2; j++){
		               str2 = assoKeyAry[i][j];

		               if(str1 == str2){
		                   lElm2 = localElm[i];
		                   lElm = i;

		                   vStr = keys[lElm2];
		                   cout<<"[ "<<str1<<" ] is part of "<<vStr<<
                                 "["<<lElm<<"] at elm [ "<<j-1<<" ]"<<endl;
		                   ck = true; break;
		               }
		           }
		           if(ck){ break; }

                 }// end for(int i=0; i<lCnt; i++)
		      }//    end if(!ck)

              if(!ck){

		         str1 = localData[0];
		         lCnt = noKeyAry.size();

		         for(int i=0; i<lCnt; i++){
		             lCnt2 = noKeyAry[i].size();

		           for(int j=0; j<lCnt2; j++){
		               str2 = noKeyAry[i][j];

		               if(str1 == str2){


		                   vStr = noKeyAryName[i][j];
		                   cout<<"[ "<<str1<<" ] is found at "<<vStr<<
                                 "["<<i<<"]["<<j<<"]"<<endl;
		                   ck = true; break;
		               }
		           }
		           if(ck){ break; }

                 }// end for(int i=0; i<lCnt; i++)
		      }//    end if(!ck)
		   }//       end else
		}//          end if(cnt < 2)

		if(localData.size() > 0){ localData.clear(); }
		if(localStr.size() > 0){ localStr.clear(); }
		if(localElm.size() > 0){ localElm.clear(); }

		return ck;
	}

	bool find2Keys()
	{
		bool ck = false;
		string str1 = "";
		string str2 = "";
		string str3 = "";
		string str4 = "";
		string str5 = "";
		vInt = 0;

		cnt2 = qList.size();

        if(cnt2 < 3){

		   if(BracketFound){

		    // user last typed keyword
		       str1 = qList[cnt2-1];

		       if(bracketID.size() > 1){

				   if(dta.size() > 0){ dta.clear(); }
				   ck = MultiBracket();
               }
               else{

				   if(dta.size() > 0){ dta.clear(); }
				   ck = SingleBracket();
			   }
		   }
		   else{

		     for(unsigned int i=0; i<qList.size(); i++){

		      // user typed 1st keyword
			     tmp1 = qList[i];

			     for(unsigned int j=0; j<keys.size(); j++){

			      // keyword from file
				     tmp2 = keys[j];

				     if(tmp1 == tmp2){

				      // user typed 2nd keyword
					     tmp1 = qList[i+1];

					     for(unsigned int k=j+1; k<keys.size(); k++){

					      // keyword from file
						     tmp2 = keys[k];

						     if(tmp1 == tmp2){

						     /* store the data from file
						        to class public variable */
						        vStr = keyData[k];
						        cout<<"current value "<<vStr<<endl;

                                setDataPosition("keyData", k);

						        elmCK1 = i;
						        elmCK2 = k;
						        ck = true; break;
						      }
					     }

				      }
				      if(ck){ break; }
		         }
		         if(ck) { break; }
		     }

		   }
		}
		return ck;

	}

	bool find3Keys()
	{
		bool ck = false;
		string str1 = "";
		string str2 = "";
		string str3 = "";
		string str4 = "";
		int lElm = 0;
		cnt2 = qList.size();

		if(BracketFound){

		    str1 = qList[cnt2-1];

		    if(bracketID.size() > 1){

				if(dta.size() > 0){ dta.clear(); }
				MultiBracket();
            }
            else{

				if(dta.size() > 0){ dta.clear(); }
				SingleBracket();
			}

		}
		else{

		  lElm = keys.size();

		  for(int i=0; i<cnt2; i++){

	  	   // user typed  1st keyword
			  tmp1 = qList[i];

			  for(int j=0; j<lElm; j++){

			   // keyword from file
				  tmp2 = keys[j];

				  if(tmp1 == tmp2){

				   // user typed 2nd keyword
					  tmp1 = qList[i+1];

					  for(int k=j+1; k<lElm; k++){

					   // keyword from file
						  tmp2 = keys[k];

						  if(tmp1 == tmp2){

						  // user typed 3rd keyword
							 tmp1 = qList[i+2];

							 for(int l=k+1; l<lElm; l++){

							 // keyword from file
								tmp2 = keys[l];

								if(tmp1 == tmp2){

							    /* storing keyword data into
							       class public variable	*/
								   vStr = keyData[l];

						           cout<<"current value "<<vStr<<endl;

						           setDataPosition("keyData", l);
						           elmCK1 = i;
						           elmCK2 = l;
						           ck = true;
						           break;
						        }
						     } //  end for(int l=k+1; l<lElm; l++)
						  } //     end if(tmp1 == tmp2)
					  } //         end for(int k=j+1; k<lElm; k++)

				  } //             end if(tmp1 == tmp2)

				  if(ck){ break; }
		      }
		      if(ck) { break; }
		  }
		}

		return ck;
	}

	bool findKeyData_Key()
	{
		bool ck = false;
		bool hasbrkt = false;

		vector<string> localData;
		vector<string> localStr;
        string str1, str2;
        str1 = str2 = "";

		int lCnt = 0;
		int lCnt2 = 0;
		int lCnt3 = 0;
		int lElm = 0;
        vector <int> localElm;

		lCnt = qList.size();
        localData.resize(lCnt, " ");
        localStr.resize(2, "\"");

        for(int i=0; i<lCnt; i++){
           localData[i] = qList[i];
        }

     // founding out how many key arrays there are
		lCnt = keyData.size();
		for(int i=0; i<lCnt; i++){
		   str1 = keyData[i];
		   //lCnt2 = str1.size();

		   str2 = str1[0];
		   if(str2 == "["){
		      localElm.push_back(i);
		   }
		}

		lCnt = qList.size();
        for(int i=0; i<lCnt; i++){
            str1 = localData[i];
            str2 = str1[0];

            if(str2 == "["){
               str1.erase(0, 1);
               str1.erase(str1.size()-1, 1);

               istringstream (str1) >> lElm;
               hasbrkt = true; break;
            }
        }

     /* putting "" around the user typed
        keywords if they don't have them */

        for(int i=0; i<lCnt; i++){

           str1 = localData[i];
           str2 = str1[0];
           if(str2 == "\""){
              localData[i] = str1;
           }
           else{
              localStr[1] = localStr[0] + str1 + localStr[0];
              localData[i] = localStr[1];
           }
        }

        for(int i=0; i<lCnt; i++){

		 // user typed keyData word
			tmp1 = localData[i];

			for(unsigned int j=0; j<keyData.size(); j++){

			 // keyData from file
				tmp2 = keyData[j];

				if(tmp1 == tmp2){

				 // user typed keyword
					tmp1 = localData[i+1];
                    lCnt2 = keys.size();

					for(int k=j+1; k<lCnt2; k++){

				     // keyword from file
						tmp2 = keys[k];

						if(tmp1 == tmp2){

                           if(hasbrkt){

                              lCnt3 = localElm.size();
                              for(int l=0; l<lCnt3; l++){
                                 if(k == localElm[l]){

                                    elmCK1 = l;
                                    elmCK2 = ++lElm;
                                    vStr = assoKeyAry[elmCK1][elmCK2];
                                    cout<<"current value [ "<< vStr<<" ]"<<endl;
                                    setDataPosition("assoAry", localElm[l]);
                                    ck = true; break;

                                 }
                              }// end for(int l=0; l<lCnt3; l++)
                           }
                           else{

					        /* storing keyData into
					           class public variable */
					           vStr = keyData[k];
						       cout<<"current value "<<vStr<<endl;
						       setDataPosition("keyData", k);

						       elmCK1 = i;
						       elmCK2 = k;
						       ck = true;
						       break;
						   }
						}
					}
					if(ck){ break; }
				}
			}
			if(ck){ break; }
		}

		if(localData.size() > 0){ localData.clear(); }
		if(localStr.size() > 0){ localStr.clear(); }

		return ck;
	}

	bool findKey_NonKey()
	{
		bool ck = false;
		int fElm = 0;

		for(unsigned int i=0; i<qList.size(); i++){

		 // user typed keyword
			tmp1 = qList[i];

		    for(unsigned int j=0; j<keys.size(); j++){

		    // keyword from file
		       tmp2 = keys[j];

               if(tmp1 == tmp2){

			   // user typed nonkey value
				  tmp1 = qList[i+1];

				  for(unsigned int k=0; k<noKeyAry.size(); k++){

				     cnt = noKeyAry[k].size();

                     if(cnt > 2){
					    for(int l=0; l<cnt; l++){
						    tmp2 = noKeyAry[k][l];

						    if(tmp1 == tmp2){

							/* retrieving the jsDataMod elm
							   and converting it for later use
							   as well as display */

							   vStr = noKeyAry[k][cnt-1];
							   istringstream (vStr) >> fElm;

							   vStr = jsDataMod[fElm][0];
						       setDataPosition("jsDataMod", fElm);

						       cout<<vStr<<endl;
						       cout<<"located at ["<<k<<"]["<<l<<"]\n";
						       ck = true;
						       break;
						    }
					    }
				     }
				     if(ck){ break; }
                  }
			   }
			   if(ck){ break; }
	        }
	        if(ck){ break; }
	    }
	    return ck;
	}

	void mathConversion(vector <string> &localData, int x, int setNums)
	{
	      string strOP, str1, str2;
          int lBeg, lEnd;
          cnt3 = cnt4 = cnt5 = cnt6 = cnt7 = 0;
          bool ck1 = false;

          strOP = str1 = "";

	      for(unsigned int i=2; i<localData.size(); i++){
	         str1 = localData[i];
	         if(str1 == "("){
	            if(!ck1){ ck1 = true; }
	            openPar.push_back(i);
             }
             if(str1 == ")"){ closePar.push_back(i); }
	      }

	      if(ck1){

	         for(unsigned int i=0; i<closePar.size(); i++){

	             if(i>0){break;}

	             lBeg = openPar[i];
	             lEnd = localData.size()-1;

	             for(int j=lBeg; j<lEnd; j++){
	                 strOP = localData[j+1];

	                 if((strOP != "+") && (strOP != "-") &&
	                    (strOP != "*") && (strOP != "/") &&
	                    (strOP != ")")){

                        if((cnt3 == 0) && (lBeg < 3)){

                        // converting to integer
                           strOP = localData[j+1];
                           istringstream (strOP) >> cnt3;
                        }
                        else if((cnt4 == 0) && (lBeg < 6)){

                        // converting to integer
                           strOP = localData[j+1];
                           istringstream (strOP) >> cnt4;
                        }
                        else if(cnt5 == 0){

                        // converting to integer
                           strOP = localData[j+1];
                           istringstream (strOP) >> cnt5;
                        }
                        else if(cnt6 == 0){

                        // converting to integer
                           strOP = localData[j+1];
                           istringstream (strOP) >> cnt6;
                        }
                        else if(cnt7 == 0){

                        // converting to integer
                           strOP = localData[j+1];
                           istringstream (strOP) >> cnt7;
                        }
	                 }

	             } // end for(int j=lBeg; j<lEnd; j++)
	         } // end for(unsigned int i=0; i<closePar.size(); i++)

	      } // end if(strOP == "=")
	      else{

          // converting to integer
             strOP = localData[x-1];
             istringstream (strOP) >> cnt3;

             strOP = localData[x+1];
             istringstream (strOP) >> cnt4;

             strOP = localData[x+3];
             istringstream (strOP) >> cnt5;

             if(setNums >= 3){
                strOP = localData[x+5];
                istringstream (strOP) >> cnt6;
             }else{ cnt6 = 0; }

             if(setNums == 4){
                strOP = localData[x+7];
                istringstream (strOP) >> cnt7;
             }else{ cnt7 = 0; }
          }
	}

	void mathmatics()
	{
		bool mathExit = false;
		string mathQuery;

		while(!mathExit){

			titleScreen(2);
            cout<<"Enter here => ";
		    getline(cin, mathQuery);
		    mathExit = parseMathData(mathQuery);

		    if(command == "quit"){
			   mathExit = true;
			}
		}

		titleScreen(1);
	}

	void nonKeyCheck()
	{
       /*****************
        * Non-Key Array *
        *****************
        Checking to see if "[" at the beginning of each line. If so,
        see if it is an array. Found out how many elements are in
        the array. Finally see if the array has a "," at the end */

        int bElm, SectEnd, lCnt, lCnt2, lCnt3;
        bElm = SectEnd = lCnt = lCnt2 = lCnt3 = 0;
        int lDtaCnt = 0;

        vector<int> localElm;
        vector<int> localStop;
        vector<int> localBegin;

        string str1, str2, str3, tmpStr;
        str1 = str2 = str3 = line2 = tmpStr = "";
/*
		for(int i=0; i<row; i++){
		   line = jsDataMod[i][0];
		   str1 = line[0];
		   myString = jsDataMod[i][0];

           if(str1 == "["){

			  for(int j=i; j<row; j++){

				  line2 = jsDataMod[j][0];
				  str2 = line2[line2.size()-1];

				  if(str2 == "]"){
					 bElm = i;
					 SectEnd = j+1;
					 ck = true; break;
				  }
			  }
		   }
		   if(ck){ break; }

        } // for(int i=0; i<row; i++)
*/
        if(dta.size() > 0){ dta.clear(); }

     // Getting total noKeyAry's count for list
	    for(int i=0; i<row; i++){
		   line = jsDataMod[i][0];
		   str1 = line[0];
		   myString = jsDataMod[i][0];

           if(str1 == "["){
              lCnt++;
              sprintf(ConvNum, "%d ", i);
              tmpStr += ConvNum;
           }
	    }

        stringstream ss1(tmpStr);
        while(ss1 >>buf1){
           dta.push_back(buf1);
        }

     // converting the str array to int array
        lCnt2 = dta.size();
        for(int i=0; i<lCnt2; i++){
           str1 = dta[i];
           istringstream (str1) >> cnt7;
           localElm.push_back(cnt7);

           if(i==0){
              localBegin.push_back(cnt7);
           }
        }
        if(dta.size() > 0){ dta.clear(); }

     /* finding the ending section
        of arrays. Like (DVDs, games) */

        tmpStr = "";
        for(int i=0; i<lCnt2; i++){
            cnt7 = localElm[i];
            lCnt3 = localElm[i+1];

            if(cnt7+1 != lCnt3){
               sprintf(ConvNum, "%d ", cnt7+1);
               tmpStr += ConvNum;
               localBegin.push_back(lCnt3);
            }
        }

        stringstream ss2(tmpStr);
        while(ss2 >>buf1){
           dta.push_back(buf1);
        }

     // converting the stop str array to int array
        lCnt2 = dta.size();
        for(int i=0; i<lCnt2; i++){
           str1 = dta[i];
           istringstream (str1) >> cnt7;
           localStop.push_back(cnt7-1);
        }

        if(localBegin.size() > localStop.size()){
           localBegin.erase(localBegin.end()-1);
        }

	 // clearing out old elements if they exists.
		if(dta.size() != 0){ dta.clear(); }

		for(unsigned int k=0; k<localBegin.size(); k++){

           bElm = localBegin[k];
           SectEnd = localStop[k];

           for(int i=bElm; i<SectEnd+1; i++){

			   str3 = "";
			   line = jsDataMod[i][0];
			   str2 = line[line.size()-1];

		    // finding the end of each array
			   if(str2 == ","){
			      lCnt = line.size()-2;
			   }
			   else{
			      lCnt = line.size()-1;
			   }

			   for(int j=1; j<lCnt; j++){
			      str3 += line[j];
			   }

		    /* split by spaces and removing
		       the "," in the string. then
		       storing the data tmp array */

               stringstream ss1(str3);
               while(ss1 >>buf1){

			      tmp1 = tmp2 = "";
			      lCnt = buf1.size();

			      for(int j=0; j<lCnt; j++){

				     tmp1 = buf1[j];

				     if(tmp1 != ","){
				       tmp2 += buf1[j];
				     }
				     else{
					   buf1 = tmp2;
				     }
			      }
			      dta.push_back(buf1);
			   }

		    /* storing the original location of data
		       to be used later for modifying the
		       data later. */

			   sprintf(ConvNum, "%d", i);
			   dta.push_back(ConvNum);
               noKeyAry.push_back(dta);
               lDtaCnt += dta.size();

	        // clearing out old elements if they exists.
		       if(dta.size() != 0){ dta.clear(); }
           }

           noKeyAryCnt.push_back(lDtaCnt);
           lDtaCnt = 0;
        }
	}

	void nonKeyNameCheck()
	{
	    int lCnt, lCnt2, lCnt3;
	    lCnt = lCnt2 = lCnt3 = 0;
        int lUpCnt, lBeg, lEnd;
        lUpCnt = lBeg = lEnd = 0;

	    string str1, str2, str3;
	    str1 = str2 = str3 = "";

	    vector< vector<string> > lnoKeyAryName;

	    if(noKeyAry.size() > 0){

	       if(noKeyAryName.size() > 0){
	          noKeyAryName.clear();
           }


        // Getting the name of the NonKeyed arrays
           lCnt = keys.size();
           lCnt2 = jsDataMod.size();

           for(int i=0; i<lCnt; i++){
              str1 = keys[i];

              if((i+1) >= lCnt2){ break; }

              str2 = jsDataMod[i+1][0];
	          str3 = str2[0];

	          if((str1 != "") && (str3 == "[")){

                  if(dta.size() > 0){ dta.clear(); }

                  dta.push_back(keys[i]);
                  sprintf(ConvNum, "%d", i);
                  dta.push_back(ConvNum);

                  lnoKeyAryName.push_back(dta);
	          }
           }

	       if(dta.size() > 0){ dta.clear(); }

	       lCnt = noKeyAry.size();
	       lCnt2 = lnoKeyAryName.size();
	       noKeyAryName.resize(lCnt);

	    // making a list for names of nonKeyAry's
	       for(int i=0; i<lCnt; i++){
	          noKeyAryName[i].resize(noKeyAry[i].size(), "");
	       }

	       for(int i=0; i<lCnt2; i++){

               str1 = lnoKeyAryName[i][0];
               for(int j=lBeg; j<lCnt; j++){

                  for(unsigned k=0; k<noKeyAry[j].size(); k++){
                     noKeyAryName[j][k] = str1;
                     lUpCnt++;
                  }

                  if(lUpCnt == noKeyAryCnt[i]){
                     lBeg = j+1;
                     lUpCnt = 0;
                  }
               }
	       }

		}//  end if(noKeyAry.size() > 0)

		if(lnoKeyAryName.size() > 0){ lnoKeyAryName.clear(); }
		if(dta.size() > 0){ dta.clear(); }
	}

	void parseData(string qStr)
	{
		int dotCnt = 0;
		int EQCK = 0;
		int lCnt = 0;
		string str1, str2;
		cnt = qStr.size();
		cnt2 = cnt3 = 0;

		if(qList.size() > 0){ qList.clear(); }
		if(qList2.size() > 0){ qList2.clear(); }

     /* removing the spaces in
        the passed in string */
		for(int i=0; i<cnt; i++){
			tmp1 = qStr[i];

			if(i == (cnt-1)){
				tmp2 += tmp1;
				qList.push_back(tmp2);
				qList2.push_back(tmp2);
				tmp1 = tmp2 = "";
			}
			else if(tmp1 != " "){
                tmp2 += tmp1;
			}
			else{
				qList.push_back(tmp2);
				qList2.push_back(tmp2);
				tmp1 = tmp2 = "";
			}
		}


     /* testing for a dot in string */
		cnt = qList.size();

     /* testing for a dot in string */
		for(int i=1; i<cnt; i++){
			tmp1 = qList[i];
			cnt2 = tmp1.size();

			for(int j=0; j<cnt2; j++){
			    tmp2 = tmp1[j];
			    if(tmp2 == "."){
				   dotCnt++;
				}
			}
		}

     // setting command before array get cleared
		command = qList[0];


 	 /* dot(s) were found in string.
 	    strip the string into dta array */

		if(dotCnt > 0){

		   cnt = qList.size() + cnt3;

		   if(dta.size() > 0){
			  dta.clear();
		   }

		   str1 = qList[1];
		   cnt = str1.size();

		   for(int i=0; i<cnt; i++){
			  tmp1 = str1[i];

			  if(i == (cnt-1)){
				 tmp2 += tmp1;
				 dta.push_back(tmp2);
				 tmp2 = "";
			  }
			  else if(tmp1 != "."){
                 tmp2 += tmp1;
			  }
			  else{
				 dta.push_back(tmp2);
				 tmp2 = "";
			  }
		   }

		   hasDot = true;
		   qList.clear();
		   cnt2 = dta.size();
		   tmp2 = "";

	    // remove extra character at the beginning
		   for(int j=0; j<cnt2; j++){

			  if(j == 0){
			     str1 = dta[j];
                 str1.erase(0, 1);
                 dta[j] = str1;
			     break;
			  }
		   }


	    // checking to see if the string has []
		   for(int i=0; i<cnt2; i++){

			  tmp1 = dta[i];
			  lCnt = tmp1.size();
			  cnt3 = 0;
			  str1 = str2 = tmp2 = tmp3 = "";

			  for(int j=0; j<lCnt; j++){
				 tmp2 = tmp1[j];

				 if(tmp2 == "["){
					for(int k=j+1; k<lCnt; k++){
					   str2 = tmp1[k];

					   if(str2 != "]"){
						  str1 += str2;
					   }
					   else{ break; }
				    }

					sscanf(str1.c_str(), "%d", &cnt3);

					str1 = "";
					bracketID.push_back(cnt3);

					if(!BracketFound){
					  dta[i] = tmp3;
					  BracketFound = true;
					}
				 }
				 else{
					tmp3 += tmp2;
				 }
			  }

		   }

		// adding " around the keywords for searches
		   for(unsigned int i=0; i<dta.size(); i++){

			  sprintf(ConvNum, "\"%s\"", dta[i].c_str());
			  qList.push_back(ConvNum);
		   }

		   cnt3 = qStr.size();
		   str1 = str2 = tmp1 = "";
		   passData = qList2[qList2.size()-1];

		   if(dta.size() > 0) { dta.clear(); }
	       if(AccessData.size() > 0){ AccessData.clear(); }

		} //  if(dotCnt > 0)


	 /* check for an "=" in qList array
	    if it's found get the ending data.
	    erase it and the "=" sign from
	    the list */

        EQCK = qList.size();
        bool popData = false;

        for(int i=1; i<EQCK; i++){
		   tmp1 = qList[i];

		   if(tmp1 == "="){
			  passData = qList[i+1];
			  popData = true; break;
		   }
		}

		if(popData){
		   qList.pop_back();
		   qList.pop_back();
		}
	}

	bool parseMathData(string qStr)
	{
	    bool ck = false;
	    bool ck1 = false;
	    bool brktfnd = false;
		int dotCnt = 0;
		int cmboMath = 0;
		int EQCK, lCnt, lCnt2, lElm, mathResult_Int;
		int lEnd;

		string str1, str2, mathResult_Str, strOP, lAns;
		vector <string> localData;

		cnt = qStr.size();
		cnt2 = cnt3 = EQCK = lCnt = 0;
		lCnt2 = lElm = mathResult_Int = 0;
		addCnt = subCnt = mulCnt = divCnt = lEnd = 0;
		str1 = str2 = mathResult_Str = strOP = lAns = "";

		if(qList.size() > 0){ qList.clear(); }

     /* removing the spaces in
        the passed in string */
        stringstream ss1(qStr);

        while(ss1 >>buf1){
		   qList.push_back(buf1);
		   localData.push_back(buf1);
		}

		lCnt = qList.size();

        for(int i=0; i<lCnt; i++){
            str1 = qList[i];

            if(str1 == "+=" || str1 == "="){
                for(int j=i; j<lCnt; j++){
                    mathStr.push_back(qList[j]);
                    lCnt2 += 1;
                }
                break;
            }
        }

        if(mathStr.size() > 0){

           for(int i=0; i<lCnt2; i++){ qList.pop_back(); }

           qStr = qList[qList.size()-1];
		   lCnt = qStr.size();

        /* testing for a dot in string */
		   for(int i=0; i<lCnt; i++){
               str1 = qStr[i];
			   if(str1 == "."){
			      dotCnt++;
			   }

		   }

 	    /* dot(s) were found in string.
 	       strip the string into dta array */

		   if(dotCnt > 0){

		      if(dta.size() > 0){
			     dta.clear();
		      }

		      for(int i=0; i<lCnt; i++){
			     str1 = qStr[i];

			     if(i == (lCnt-1)){
				    str2 += str1;
				    dta.push_back(str2);
				    str2 = "";
			     }
			     else if(str1 != "."){
                    str2 += str1;
			     }
			     else{
				    dta.push_back(str2);
				    str2 = "";
			     }
		      }

		      hasDot = true;
		      qList.clear();
		      cnt2 = dta.size();
		      str2 = "";

	       // checking to see if the string has []
		      for(int i=0; i<cnt2; i++){

			     str1 = dta[i];
			     lCnt = str1.size();
			     cnt3 = 0;
			     str2 = tmp2 = tmp3 = "";

			     for(int j=0; j<lCnt; j++){
				    str2 = str1[j];

				    if(str2 == "["){
					   for(int k=j+1; k<lCnt; k++){
					      str2 = str1[k];

					      if(!brktfnd){
					         sscanf(str2.c_str(), "%d", &cnt3);
					         brktfnd = true;
					      }

					      if(str2 != "]"){
						     str1 += str2;
					      }
					      else{ break; }
				       }

				       if(!brktfnd){
					      sscanf(str1.c_str(), "%d", &cnt3);
					   }

					   str1 = "";
					   bracketID.push_back(cnt3);

					   if(!BracketFound){
					     dta[i] = tmp3;
					     BracketFound = true;
					   }
				    }
				    else{
					   tmp3 += str2;
				    }
			     }

		      }


		   // adding " around the keywords for searches
		      for(unsigned int i=0; i<dta.size(); i++){

			     sprintf(ConvNum, "\"%s\"", dta[i].c_str());
			     qList.push_back(ConvNum);
		      }

		      cnt3 = 0;
		      str1 = str2 = tmp1 = "";

           /* checking to see if ^ was found in the string. the
              users wants to use a variable's data in the equation */

		      for(unsigned int i=0; i<mathStr.size(); i++){
		          str1 = mathStr[i];

		          for(unsigned int j=0; j<str1.size(); j++){
                      str2 = str1[j];

                      if(str2 == "^"){
                          str2 = "";
                          for(unsigned int k=j+1; k<str1.size(); k++){
                              str2 += str1[k];
                          }

                          updateUserVars();
                          for(unsigned int k=0; k<txtFileVar.size(); k++){
                             if(str2 == txtFileVar[k][0]){
                                 mathStr[i] = txtFileVar[k][1];
                                 break;
                             }
                          }
                          break;
                      }
		          }

                  for(unsigned int j=1; j<localData.size(); j++){
		             if(localData[j] == mathStr[j-1]){

                        for(unsigned int k=j; k<mathStr.size(); k++){
                            localData[k+1] = mathStr[k];
                            ck = true;
                        }
                        break;
		             }
		             if(ck){ break; }
		          }
		      }

              ck = false;
		      mathResult_Str = mathStr[mathStr.size()-1];
		      passData = mathResult_Str;
		      istringstream (passData) >> mathResult_Int;

           // clearing out old data
		      if(dta.size() > 0) { dta.clear(); }
	          if(AccessData.size() > 0){ AccessData.clear(); }
	          if(mathStr.size() > 0){ mathStr.clear(); }

	          find(hasDot);

	          for(unsigned int i=0; i<localData.size(); i++){
	              str1 = localData[i];

	              if((str1 == "+=") || (str1 == "=")){
                     lCnt = i;
                     break;
	              }
	          }

	          for(unsigned int i=lCnt; i<localData.size(); i++){
                  str1 = localData[i];

	              if(str1 == "+="){
                     vInt += mathResult_Int;
		             ck = true; break;
	              }
	              else if(str1 == "-="){
                     vInt -= mathResult_Int;
		             ck = true; break;
	              }
	              else if(str1 == "*="){
                     vInt *= mathResult_Int;
		             ck = true; break;

	              }
	              else if(str1 == "/="){

                     vInt = vInt/mathResult_Int;
		             ck = true; break;
	              }
	              else if(str1 == "="){

                     for(unsigned int j=i+1; j<localData.size(); j++){
                         strOP = localData[j];
                         if(strOP == "+"){ addCnt += 1; }
                         if(strOP == "-"){ subCnt += 1; }
                         if(strOP == "*"){ mulCnt += 1; }
                         if(strOP == "/"){ divCnt += 1; }
                     }

                     if((addCnt > 0) && (subCnt == 0) &&
                        (mulCnt == 0) && (divCnt == 0)){

                         addCnt = 0;
                         for(unsigned int j=i+1; j<localData.size(); j++){
                             strOP = localData[j];


                             if(strOP != "+"){
                                istringstream (strOP) >> cnt3;
                                addCnt += cnt3;
                             }
                         }
                         vInt = addCnt;
		                 ck = true; break;
                     }
                     else if((addCnt == 0) && (subCnt > 0) &&
                             (mulCnt == 0) && (divCnt == 0)){

                             for(unsigned int j=i+1; j<localData.size(); j++){
                                 strOP = localData[j];

                                 subCnt = 0;
                                 if(strOP != "-"){
                                    istringstream (strOP) >> cnt3;
                                    if(subCnt == 0){ subCnt = cnt3; }
                                    else{ subCnt -= cnt3; }
                                 }
                             }
                             vInt = subCnt;
		                     ck = true; break;
                     }
                     else if((addCnt == 0) && (subCnt == 0) &&
                             (mulCnt > 0) && (divCnt == 0)){

                             for(unsigned int j=i+1; j<localData.size(); j++){
                                strOP = localData[j];

                                mulCnt = 0;
                                if(strOP != "*"){
                                   istringstream (strOP) >> cnt3;
                                   if(mulCnt == 0){ mulCnt = cnt3; }
                                   else{ mulCnt *= cnt3; }
                                }
                            }
                            vInt = mulCnt;
                            ck = true; break;
                     }
                     else if((addCnt == 0) && (subCnt == 0) &&
                             (mulCnt == 0) && (divCnt > 0)){

                            for(unsigned int j=i+1; j<localData.size(); j++){
                                strOP = localData[j];

                                divCnt = 0;
                                if(strOP != "/"){
                                   istringstream (strOP) >> cnt3;
                                   if(divCnt == 0){ divCnt = cnt3; }
                                   else{ divCnt /= cnt3; }
                                }
                            }
                            vInt = divCnt;
		                    ck = true; break;
                     }

                    /*****************************
                       Multi-Operational section
                     *****************************/

                     else if((addCnt > 0) && (subCnt > 0) &&
                             (mulCnt == 0) && (divCnt == 0)){

                            lElm = i+1;

                            for(unsigned int j=lElm; j<localData.size(); j++){
                                strOP = localData[j];

                                if((strOP == "+") || (strOP == "-") || (strOP == "(")){

                                   if((addCnt + subCnt) == 2){

                                      mathConversion(localData , j, 2);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 2);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 2, j);
                                      }
                                      break;
                                   }

                                   else if((addCnt + subCnt) == 3){

                                      mathConversion(localData , j, 3);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 3);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 3, j);
                                      }

                                      break;
                                   }

                                   else if((addCnt + subCnt) == 4){

                                      mathConversion(localData , j, 4);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 4);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 4, j);
                                      }

                                      break;
                                   }

                                }//   end if((strOP == "+") || (strOP == "-") || (strOP == "("))
                            }//       end for(unsigned int j=lElm; j<localData.size(); j++)

                            vInt = cmboMath;
		                    ck = true; break;
                     }
                     else if((addCnt > 0) && (subCnt == 0) &&
                             (mulCnt > 0) && (divCnt == 0)){

                            lElm = i+1;

                            for(unsigned int j=lElm; j<localData.size(); j++){
                                strOP = localData[j];

                                if((strOP == "+") || (strOP == "*") || (strOP == "(")){

                                   if((addCnt + mulCnt) == 2){

                                      mathConversion(localData , j, 2);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 2);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 2, j);
                                      }
                                      break;
                                   }
                                   if((addCnt + mulCnt) == 3){

                                      mathConversion(localData , j, 3);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 3);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 3, j);
                                      }

                                      break;
                                   }
                                   if((addCnt + mulCnt) == 4){

                                      mathConversion(localData , j, 4);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 4);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 4, j);
                                      }

                                      break;
                                   }

                                }//   end if((strOP == "+") || (strOP == "*") || (strOP == "("))
                            }//       end for(unsigned int j=lElm; j<localData.size(); j++)
                            vInt = cmboMath;
		                    ck = true; break;
                     }
                     else if((addCnt > 0) && (subCnt == 0) &&
                             (mulCnt == 0) && (divCnt > 0)){

                            lElm = i+1;

                            for(unsigned int j=lElm; j<localData.size(); j++){
                                strOP = localData[j];

                                if((strOP == "+") || (strOP == "/") || (strOP == "(")){
                                   if((addCnt + divCnt) == 2){

                                      mathConversion(localData , j, 2);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 2);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 2, j);
                                      }

                                      break;
                                   }
                                   else if((addCnt + divCnt) == 3){
                                      if((strOP == "+") || (strOP == "/") ||
                                         (strOP == "(")){

                                         mathConversion(localData , j, 3);

                                         if(openPar.size() > 0){ ck1 = true; }

                                         if(ck1){
                                            cmboMath = mathParentheses(localData, 3);
                                         }

                                      /* No ()'s so normal math from left to right
                                         with M.D.A.S method */
                                         else{
                                            cmboMath = math_NonParentheses(localData, 3, j);
                                         }
                                      }
                                      break;
                                   }
                                   else if((addCnt + divCnt) == 4){

                                      if((strOP == "+") || (strOP == "/") ||
                                         (strOP == "(")){

                                         mathConversion(localData , j, 4);

                                         if(openPar.size() > 0){ ck1 = true; }

                                         if(ck1){
                                            cmboMath = mathParentheses(localData, 4);
                                         }

                                      /* No ()'s so normal math from left to right
                                         with M.D.A.S method */
                                         else{
                                            cmboMath = math_NonParentheses(localData, 4, j);
                                         }
                                      }
                                      break;
                                   }

                                }/*   end if((strOP == "+") || (strOP == "/") ||
                                             (strOP == "(")) */

                            }//       end for(unsigned int j=lElm; j<localData.size(); j++)
                            vInt = cmboMath;
		                    ck = true; break;
                     }
                     else if((addCnt > 0) && (subCnt > 0) &&
                             (mulCnt == 0) && (divCnt > 0)){

                            lElm = i+1;

                            for(unsigned int j=lElm; j<localData.size(); j++){
                                strOP = localData[j];

                                if((strOP == "+") || (strOP == "-") ||
                                   (strOP == "/") || (strOP == "(")){

                                   if((addCnt + subCnt + divCnt) == 3){

                                      mathConversion(localData , j, 3);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 3);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 3, j);
                                      }

                                      break;
                                   }
                                   if((addCnt + subCnt + divCnt) == 4){

                                      mathConversion(localData , j, 4);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 4);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 4, j);
                                      }

                                      break;
                                   }

                                }/*   end if((strOP == "+") || (strOP == "-") ||
                                             (strOP == "/") || (strOP == "(")) */

                            }//       end for(unsigned int j=lElm; j<localData.size(); j++)
                            vInt = cmboMath;
		                    ck = true; break;
                     }
                     else if((addCnt > 0) && (subCnt == 0) &&
                             (mulCnt > 0) && (divCnt > 0)){

                            lElm = i+1;

                            for(unsigned int j=lElm; j<localData.size(); j++){
                                strOP = localData[j];

                                if((strOP == "+") || (strOP == "*") ||
                                   (strOP == "/") || (strOP == "(")){

                                   if((addCnt + mulCnt + divCnt) == 3){

                                      mathConversion(localData , j, 3);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 3);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 3, j);
                                      }

                                      break;
                                   }
                                   if((addCnt + mulCnt + divCnt) == 4){

                                      mathConversion(localData , j, 4);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 4);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 4, j);
                                      }

                                      break;
                                   }

                                }/*   end if((strOP == "+") || (strOP == "-") ||
                                             (strOP == "/") || (strOP == "(")) */

                            }//       end for(unsigned int j=lElm; j<localData.size(); j++)
                            vInt = cmboMath;
		                    ck = true; break;
                     }
                     else if((addCnt == 0) && (subCnt > 0) &&
                             (mulCnt > 0) && (divCnt == 0)){

                            lElm = i+1;

                            for(unsigned int j=lElm; j<localData.size(); j++){
                                strOP = localData[j];

                                if((strOP == "+") || (strOP == "*") || (strOP == "(")){
                                   if((mulCnt + subCnt) == 2){

                                      mathConversion(localData , j, 2);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 2);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 2, j);
                                      }
                                      break;
                                   }
                                   if((mulCnt + subCnt) == 3){

                                      mathConversion(localData , j, 3);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 3);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 3, j);
                                      }

                                      break;
                                   }
                                   if((mulCnt + subCnt) == 4){

                                      mathConversion(localData , j, 4);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 4);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 4, j);
                                      }

                                      break;
                                   }

                                }//   end if((strOP == "+") || (strOP == "*") || (strOP == "("))
                            }//       end for(unsigned int j=lElm; j<localData.size(); j++)
                            vInt = cmboMath;
		                    ck = true; break;
                     }
                     else if((addCnt == 0) && (subCnt == 0) &&
                             (mulCnt > 0) && (divCnt > 0)){

                            lElm = i+1;

                            for(unsigned int j=lElm; j<localData.size(); j++){
                                strOP = localData[j];

                                if((strOP == "*") || (strOP == "/") || (strOP == "(")){
                                   if((mulCnt + divCnt) == 2){

                                      mathConversion(localData , j, 2);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 2);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 2, j);
                                      }
                                      break;
                                   }
                                   if((mulCnt + divCnt) == 3){

                                      mathConversion(localData , j, 3);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 3);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 3, j);
                                      }

                                      break;
                                   }
                                   if((mulCnt + divCnt) == 4){

                                      mathConversion(localData , j, 4);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 4);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 4, j);
                                      }

                                      break;
                                   }

                                }//   end if((strOP == "*") || (strOP == "/") || (strOP == "("))
                            }//       end for(unsigned int j=lElm; j<localData.size(); j++)
                            vInt = cmboMath;
		                    ck = true; break;
                     }
                     else if((addCnt == 0) && (subCnt > 0) &&
                             (mulCnt > 0) && (divCnt > 0)){

                            lElm = i+1;

                            for(unsigned int j=lElm; j<localData.size(); j++){
                                strOP = localData[j];

                                if((strOP == "-") || (strOP == "*") ||
                                   (strOP == "/") || (strOP == "(")){

                                   if((subCnt + mulCnt + divCnt) == 3){

                                      mathConversion(localData , j, 3);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 3);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 3, j);
                                      }

                                      break;
                                   }
                                   if((subCnt + mulCnt + divCnt) == 4){

                                      mathConversion(localData , j, 4);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 4);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 4, j);
                                      }

                                      break;
                                   }

                                }/*   end if((strOP == "-") || (strOP == "*") ||
                                             (strOP == "/") || (strOP == "(")) */

                            }//       end for(unsigned int j=lElm; j<localData.size(); j++)
                            vInt = cmboMath;
		                    ck = true; break;
                     }
                     else if((addCnt > 0) && (subCnt > 0) &&
                             (mulCnt > 0) && (divCnt > 0)){

                            lElm = i+1;

                            for(unsigned int j=lElm; j<localData.size(); j++){
                                strOP = localData[j];

                                if((strOP == "+") || (strOP == "-") ||
                                   (strOP == "*") || (strOP == "/") ||
                                   (strOP == "(")){

                                   if((addCnt + subCnt + mulCnt + divCnt) >= 4){

                                      mathConversion(localData , j, 4);

                                      if(openPar.size() > 0){ ck1 = true; }

                                      if(ck1){
                                         cmboMath = mathParentheses(localData, 4);
                                      }

                                   /* No ()'s so normal math from left to right
                                      with M.D.A.S method */
                                      else{
                                         cmboMath = math_NonParentheses(localData, 4, j);
                                      }

                                      break;
                                   }

                                }/*   end if((strOP == "+") || (strOP == "-") ||
                                             (strOP == "*") || (strOP == "/") ||
                                             (strOP == "(")) */

                            }//       end for(unsigned int j=lElm; j<localData.size(); j++)
                            vInt = cmboMath;
		                    ck = true; break;
                     }

	              } // else if(str1 == "=")
	          }

		   // Converting the math result to string
		      convPubInt_2_StrVal();

           // Element for changed data
		      str2 = AccessData[0][1];
		      istringstream (str2) >> lElm;

           // keyData/jsDataMod selector
		      str2 = AccessData[0][0];

		   // changed data
		      vStr = AccessData[0][2];

		      if(str2 == "jsDataMod"){
		         noKeyAry[elmCK1][elmCK2] = vStr;
		      }
		      else if(str2 == "assoAry"){
		         assoKeyAry[elmCK1][elmCK2] = vStr;
		      }
		      else{
		         keyData[lElm] = vStr;
              }

		      updateNoKeyData();
		      updateAssoAry();
		      BeforeWriteCheck();
              WriteFile();

		   } //  if(dotCnt > 0)
		}
		else{

		   str1 = qList[0];

           if(str1 != "quit"){

	       // converting user typed data to integer
	          str2 = AccessData[0][1];
	          istringstream (str2) >> lElm;

	          str2 = qList[qList.size()-1];
		      istringstream (str2) >> lCnt2;

           // performing math function
		      if(str1 == "add"){ vInt += lCnt2; }
		      else if(str1 == "sub"){ vInt -= lCnt2; }
		      else if(str1 == "mul"){ vInt *= lCnt2; }
		      else if(str1 == "div"){ vInt /= lCnt2; }

		   // Converting the math result to string
		      convPubInt_2_StrVal();

		      str2 = AccessData[0][0];
		      vStr = AccessData[0][2];

		      if(str2 == "jsDataMod"){
		         noKeyAry[elmCK1][elmCK2] = vStr;
		      }
		      else if(str2 == "assoAry"){
		         assoKeyAry[elmCK1][elmCK2] = vStr;
		      }
		      else{ keyData[lElm] = vStr; }

		      updateNoKeyData();
		      updateAssoAry();
		      BeforeWriteCheck();
              WriteFile();
		      ck = true;
		   }
		// user typed quit
		   else{ ck = true; }
		}

	 // clear old data
		if(openPar.size() > 0){ openPar.clear(); }
		if(closePar.size() > 0){ closePar.clear(); }

		return ck;
	}

	void loadFile()
	{
         FileName = "";
         FileBKup = "";
         clearArrays();
         Setup("", "", FileVariables, FileUserFiles);
	}

	int mathParentheses(vector <string> &localData, int lStep)
	{
	     string strOP = "";
	     string str1 = "";

         int lElm , lTop, lBtm, lCnt, lCnt2, cmboMath;
         lElm = lTop = lBtm = lCnt = lCnt2 = cmboMath = 0;

         if(lStep == 2){

                lCnt = openPar[0];
                lCnt2 = closePar[0];

             // Means (num op num op num)
                if((lCnt == 2) && (lCnt2 == 8)){

                    lElm = 4;
                    if(lElm == 4){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath = cnt3 + cnt4;
                       }
                       else if(strOP == "-"){
                          cmboMath = cnt3 - cnt4;
                       }
                       else if(strOP == "*"){
                          cmboMath = cnt3 * cnt4;
                       }
                       else if(strOP == "/"){
                          cmboMath = cnt3 / cnt4;
                       }
                    }

                    lElm = 6;
                    if(lElm == 6){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath += cnt5;
                       }
                       else if(strOP == "-"){
                          cmboMath -= cnt5;
                       }
                       else if(strOP == "*"){
                          cmboMath *= cnt5;
                       }
                       else if(strOP == "/"){
                          cmboMath /= cnt5;
                       }
                    }

                } // if((lCnt == 2) && (lCnt2 == 8))

             // Means (num op num) op num
                else if((lCnt == 2) && (lCnt2 == 6)){

                    lElm = 4;
                    if(lElm == 4){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath = cnt3 + cnt4;
                       }
                       else if(strOP == "-"){
                          cmboMath = cnt3 - cnt4;
                       }
                       else if(strOP == "*"){
                          cmboMath = cnt3 * cnt4;
                       }
                       else if(strOP == "/"){
                          cmboMath = cnt3 / cnt4;
                       }
                    }

                    lElm = 7;
                    if(lElm == 7){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath += cnt5;
                       }
                       else if(strOP == "-"){
                          cmboMath -= cnt5;
                       }
                       else if(strOP == "*"){
                          cmboMath *= cnt5;
                       }
                       else if(strOP == "/"){
                          cmboMath /= cnt5;
                       }
                    }
                }

             // Means num op (num op num)
                else if((lCnt == 4) && (lCnt2 == 8)){

                     if(cnt3 == 0){
                      str1 = localData[2];
                      istringstream (str1) >> cnt3;
                    }

                    lElm = 6;
                    if(lElm == 6){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath = cnt4 + cnt5;
                       }
                       else if(strOP == "-"){
                          cmboMath = cnt4 - cnt5;
                       }
                       else if(strOP == "*"){
                          cmboMath = cnt4 * cnt5;
                       }
                       else if(strOP == "/"){
                          cmboMath = cnt4 / cnt5;
                       }
                    }

                    lElm = 3;
                    if(lElm == 3){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath = cnt3 + cmboMath;
                       }
                       else if(strOP == "-"){
                          cmboMath = cnt3 - cmboMath;
                       }
                       else if(strOP == "*"){
                          cmboMath = cnt3 * cmboMath;
                       }
                       else if(strOP == "/"){
                          cmboMath = cnt3 / cmboMath;
                       }
                    }
                }
         }

         else if(lStep == 3){

           // Means (num op num) op (num op num)
              if(openPar.size() > 1){

                 lElm = 4;
                 if(lElm == 4){
                    strOP = localData[lElm];

                    if(strOP == "+"){
                       lTop = cnt3 + cnt4;
                    }
                    else if(strOP == "-"){
                       lTop = cnt3 - cnt4;
                    }
                    else if(strOP == "*"){
                       lTop = cnt3 * cnt4;
                    }
                    else if(strOP == "/"){
                       lTop = cnt3 / cnt4;
                    }
                 }

                 lElm = 10;
                 if(lElm == 10){
                    strOP = localData[lElm];

                    if(strOP == "+"){
                       lBtm = cnt5 + cnt6;
                    }
                    else if(strOP == "-"){
                       lBtm = cnt5 - cnt6;
                    }
                    else if(strOP == "*"){
                       lBtm = cnt5 * cnt6;
                    }
                    else if(strOP == "/"){
                       lBtm = cnt5 / cnt6;
                    }
                 }

                 lElm = 7;
                 if(lElm == 7){
                    strOP = localData[lElm];

                    if(strOP == "+"){
                       cmboMath = lTop + lBtm;
                    }
                    else if(strOP == "-"){
                       cmboMath = lTop - lBtm;
                    }
                    else if(strOP == "*"){
                       cmboMath = lTop * lBtm;
                    }
                    else if(strOP == "/"){
                       cmboMath = lTop / lBtm;
                    }
                 }

             } // end if(openPar.size() > 1)

          // has only 1 ()
             else{

                lCnt = openPar[0];
                lCnt2 = closePar[0];

             // Means (num op num op num) op num
                if((lCnt == 2) && (lCnt2 == 8)){

                    lElm = 4;
                    if(lElm == 4){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath = cnt3 + cnt4;
                       }
                       else if(strOP == "-"){
                          cmboMath = cnt3 - cnt4;
                       }
                       else if(strOP == "*"){
                          cmboMath = cnt3 * cnt4;
                       }
                       else if(strOP == "/"){
                          cmboMath = cnt3 / cnt4;
                       }
                    }

                    lElm = 6;
                    if(lElm == 6){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath += cnt5;
                       }
                       else if(strOP == "-"){
                          cmboMath -= cnt5;
                       }
                       else if(strOP == "*"){
                          cmboMath *= cnt5;
                       }
                       else if(strOP == "/"){
                          cmboMath /= cnt5;
                       }
                    }

                    lElm = 9;
                    if(lElm == 9){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath += cnt6;
                       }
                       else if(strOP == "-"){
                          cmboMath -= cnt6;
                       }
                       else if(strOP == "*"){
                          cmboMath *= cnt6;
                       }
                       else if(strOP == "/"){
                          cmboMath /= cnt6;
                       }
                    }
                } // end if((lCnt == 2) && (lCnt2 == 8))

             // Means (num op num) op num op num
                else if((lCnt == 2) && (lCnt2 == 6)){

                    lElm = 4;
                    if(lElm == 4){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath = cnt3 + cnt4;
                       }
                       else if(strOP == "-"){
                          cmboMath = cnt3 - cnt4;
                       }
                       else if(strOP == "*"){
                          cmboMath = cnt3 * cnt4;
                       }
                       else if(strOP == "/"){
                          cmboMath = cnt3 / cnt4;
                       }
                   }

                   lElm = 7;
                   if(lElm == 7){

                      strOP = localData[lElm];
                      if(strOP == "+"){
                         cmboMath += cnt5;
                      }
                      else if(strOP == "-"){
                         cmboMath -= cnt5;
                      }
                      else if(strOP == "*"){
                         cmboMath *= cnt5;
                      }
                      else if(strOP == "/"){
                         cmboMath /= cnt5;
                      }
                   }

                   lElm = 9;
                   if(lElm == 9){

                      strOP = localData[lElm];
                      if(strOP == "+"){
                         cmboMath += cnt6;
                      }
                      else if(strOP == "-"){
                         cmboMath -= cnt6;
                      }
                      else if(strOP == "*"){
                         cmboMath *= cnt6;
                      }
                      else if(strOP == "/"){
                         cmboMath /= cnt6;
                      }
                   }
                } // end else if((lCnt == 2) && (lCnt2 == 6))

             // Means num op (num op num) op num
                else if((lCnt == 4) && (lCnt2 == 8)){

                   lElm = 6;
                   if(lElm == 6){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath = cnt4 + cnt5;
                       }
                       else if(strOP == "-"){
                          cmboMath = cnt4 - cnt5;
                       }
                       else if(strOP == "*"){
                          cmboMath = cnt4 * cnt5;
                       }
                       else if(strOP == "/"){
                          cmboMath = cnt4 / cnt5;
                       }
                   }

                   lElm = 3;
                   if(lElm == 3){

                      if(cnt3 == 0){
                         str1 = localData[2];
                         istringstream (str1) >> cnt3;
                      }

                      strOP = localData[lElm];
                      if(strOP == "+"){
                          cmboMath += cnt3;
                      }
                      else if(strOP == "-"){

                         cmboMath = cnt3 - cmboMath;
                      }
                      else if(strOP == "*"){
                         cmboMath = cnt3 * cmboMath;
                      }
                      else if(strOP == "/"){
                         cmboMath = cnt3 / cmboMath;
                      }
                   }

                   lElm = 9;
                   if(lElm == 9){

                      strOP = localData[lElm];
                      if(strOP == "+"){
                         cmboMath += cnt6;
                      }
                      else if(strOP == "-"){
                        cmboMath -= cnt6;
                      }
                      else if(strOP == "*"){
                        cmboMath *= cnt6;
                      }
                      else if(strOP == "/"){
                        cmboMath /= cnt6;
                      }
                   }
                } // end else if((lCnt == 4) && (lCnt2 == 8))

             // Means num op (num op num op num)
                else if((lCnt == 4) && (lCnt2 == 10)){

                   if(cnt3 == 0){
                      str1 = localData[2];
                      istringstream (str1) >> cnt3;
                   }

                   lElm = 6;
                   if(lElm == 6){

                      strOP = localData[lElm];
                      if(strOP == "+"){
                         cmboMath = cnt4 + cnt5;
                      }
                      else if(strOP == "-"){
                         cmboMath = cnt4 - cnt5;
                      }
                      else if(strOP == "*"){
                         cmboMath = cnt4 * cnt5;
                      }
                      else if(strOP == "/"){
                         cmboMath = cnt4 / cnt5;
                      }
                   }

                   lElm = 8;
                   if(lElm == 8){

                      strOP = localData[lElm];
                      if(strOP == "+"){
                         cmboMath += cnt6;
                      }
                      else if(strOP == "-"){
                         cmboMath -= cnt6;
                      }
                      else if(strOP == "*"){
                         cmboMath *= cnt6;
                      }
                      else if(strOP == "/"){
                         cmboMath /= cnt6;
                      }
                   }

                   lElm = 3;
                   if(lElm == 3){

                      strOP = localData[lElm];
                      if(strOP == "+"){
                         cmboMath += cnt3;
                      }
                      else if(strOP == "-"){
                         cmboMath = cnt3 - cmboMath;
                      }
                      else if(strOP == "*"){
                         cmboMath = cnt3 * cmboMath;
                      }
                      else if(strOP == "/"){
                         cmboMath = cnt3 / cmboMath;
                      }
                   }
                } // end else if((lCnt == 4) && (lCnt2 == 10))

             // Means num op num op (num op num)
                else if((lCnt == 6) && (lCnt2 == 10)){

                    if(cnt3 == 0){
                       str1 = localData[2];
                       istringstream (str1) >> cnt3;
                    }

                    if(cnt4 == 0){
                       str1 = localData[4];
                       istringstream (str1) >> cnt4;
                    }

                    lElm = 8;
                    if(lElm == 8){

                      strOP = localData[lElm];
                      if(strOP == "+"){
                        lBtm = cnt5 + cnt6;
                      }
                      else if(strOP == "-"){
                        lBtm = cnt5 - cnt6;
                      }
                      else if(strOP == "*"){
                        lBtm = cnt5 * cnt6;
                      }
                      else if(strOP == "/"){
                        lBtm = cnt5 / cnt6;
                      }
                   }

                   lElm = 3;
                   if(lElm == 3){

                      strOP = localData[lElm];
                      if(strOP == "+"){
                         cmboMath = cnt3 + cnt4;
                      }
                      else if(strOP == "-"){
                         cmboMath = cnt3 - cnt4;
                      }
                      else if(strOP == "*"){
                         cmboMath = cnt3 * cnt4;
                      }
                      else if(strOP == "/"){
                         cmboMath = cnt3 / cnt4;
                      }
                   }

                   lElm = 5;
                   if(lElm == 5){

                      strOP = localData[lElm];
                      if(strOP == "+"){
                         cmboMath += lBtm;
                      }
                      else if(strOP == "-"){
                         cmboMath -= lBtm;
                      }
                      else if(strOP == "*"){
                         cmboMath *= lBtm;
                      }
                      else if(strOP == "/"){
                         cmboMath /= lBtm;
                      }
                   }
                }// end else if((lCnt == 6) && (lCnt2 == 10))
             } //   end else
         } //       end if(lStep == 3)

         else if(lStep == 4){

           // Means 2 ()'s in string
              if(openPar.size() > 1){

                 lCnt = openPar[0];
                 lCnt2 = closePar[0];

              // mean (num op num op num) op (num op num)
                 if((lCnt == 2) && (lCnt2 ==8)){

                    lElm = 4;
                    if(lElm == 4){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          lTop = cnt3 + cnt4;
                       }
                       else if(strOP == "-"){
                          lTop = cnt3 - cnt4;
                       }
                       else if(strOP == "*"){
                          lTop = cnt3 * cnt4;
                       }
                       else if(strOP == "/"){
                          lTop = cnt3 / cnt4;
                       }
                    }

                    lElm = 6;
                    if(lElm == 6){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          lTop += cnt5;
                       }
                       else if(strOP == "-"){
                          lTop -= cnt5;
                       }
                       else if(strOP == "*"){
                          lTop *= cnt5;
                       }
                       else if(strOP == "/"){
                          lTop /= cnt5;
                       }
                    }

                    lElm = 12;
                    if(lElm == 12){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          lBtm = cnt6 + cnt7;
                       }
                       else if(strOP == "-"){
                          lBtm = cnt6 - cnt7;
                       }
                       else if(strOP == "*"){
                          lBtm = cnt6 * cnt7;
                       }
                       else if(strOP == "/"){
                          lBtm = cnt6 / cnt7;
                       }
                    }

                    lElm = 9;
                    if(lElm == 9){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath = lTop + lBtm;
                       }
                       else if(strOP == "-"){
                          cmboMath = lTop - lBtm;
                       }
                       else if(strOP == "*"){
                          cmboMath = lTop * lBtm;
                       }
                       else if(strOP == "/"){
                          cmboMath = lTop / lBtm;
                       }
                    }
                 } // end if((lCnt == 2) && (lCnt2 ==8))

              // mean (num op num) op (num op num op num)
                 else if((lCnt == 2) && (lCnt2 == 6) &&
                         (openPar[1] == 8)){

                    lElm = 4;
                    if(lElm == 4){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          lTop = cnt3 + cnt4;
                       }
                       else if(strOP == "-"){
                          lTop = cnt3 - cnt4;
                       }
                       else if(strOP == "*"){
                          lTop = cnt3 * cnt4;
                       }
                       else if(strOP == "/"){
                          lTop = cnt3 / cnt4;
                       }
                    }

                    lElm = 10;
                    if(lElm == 10){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          lBtm = cnt5 + cnt6;
                       }
                       else if(strOP == "-"){
                          lBtm = cnt5 - cnt6;
                       }
                       else if(strOP == "*"){
                          lBtm = cnt5 * cnt6;
                       }
                       else if(strOP == "/"){
                          lBtm = cnt5 / cnt6;
                       }
                    }

                    lElm = 12;
                    if(lElm == 12){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          lBtm += cnt7;
                       }
                       else if(strOP == "-"){
                          lBtm -= cnt7;
                       }
                       else if(strOP == "*"){
                          lBtm *= cnt7;
                       }
                       else if(strOP == "/"){
                          lBtm /= cnt7;
                       }
                    }

                    lElm = 7;
                    if(lElm == 7){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath = lTop + lBtm;
                       }
                       else if(strOP == "-"){
                          cmboMath = lTop - lBtm;
                       }
                       else if(strOP == "*"){
                          cmboMath = lTop * lBtm;
                       }
                       else if(strOP == "/"){
                          cmboMath = lTop / lBtm;
                       }
                    }
                 } /* end else if((lCnt == 2) && (lCnt2 == 6) &&
                                  (openPar[1] == 8)) */

              // mean (num op num) op num op (num op num)
                 else if((lCnt == 2) && (lCnt2 == 6) &&
                         (openPar[1] == 10)){

                    lElm = 4;
                    if(lElm == 4){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          lTop = cnt3 + cnt4;
                       }
                       else if(strOP == "-"){
                          lTop = cnt3 - cnt4;
                       }
                       else if(strOP == "*"){
                          lTop = cnt3 * cnt4;
                       }
                       else if(strOP == "/"){
                          lTop = cnt3 / cnt4;
                       }
                    }

                    lElm = 12;
                    if(lElm == 12){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          lBtm = cnt6 + cnt7;
                       }
                       else if(strOP == "-"){
                          lBtm = cnt6 - cnt7;
                       }
                       else if(strOP == "*"){
                          lBtm = cnt6 * cnt7;
                       }
                       else if(strOP == "/"){
                          lBtm = cnt6 / cnt7;
                       }
                    }

                    lElm = 7;
                    if(lElm == 7){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath = lTop + cnt5;
                       }
                       else if(strOP == "-"){
                          cmboMath = lTop - cnt5;
                       }
                       else if(strOP == "*"){
                          cmboMath = lTop * cnt5;
                       }
                       else if(strOP == "/"){
                          cmboMath = lTop / cnt5;
                       }
                    }

                    lElm = 9;
                    if(lElm == 9){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath += lBtm;
                       }
                       else if(strOP == "-"){
                          cmboMath -= lBtm;
                       }
                       else if(strOP == "*"){
                          cmboMath *= lBtm;
                       }
                       else if(strOP == "/"){
                          cmboMath /= lBtm;
                       }
                    }

                 } /* end else if((lCnt == 2) && (lCnt2 == 6) &&
                                  (openPar[1] == 10)) */

              // mean num op (num op num) op (num op num)
                 else if((lCnt == 4) && (lCnt2 == 8) &&
                         (openPar[1] == 10)){

                    if(cnt3 == 0){
                       str1 = localData[2];
                       istringstream (str1) >> cnt3;
                    }

                    lElm = 6;
                    if(lElm == 6){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          lTop = cnt4 + cnt5;
                       }
                       else if(strOP == "-"){
                          lTop = cnt4 - cnt5;
                       }
                       else if(strOP == "*"){
                          lTop = cnt4 * cnt5;
                       }
                       else if(strOP == "/"){
                          lTop = cnt4 / cnt5;
                       }
                    }

                    lElm = 12;
                    if(lElm == 12){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          lBtm = cnt6 + cnt7;
                       }
                       else if(strOP == "-"){
                          lBtm = cnt6 - cnt7;
                       }
                       else if(strOP == "*"){
                          lBtm = cnt6 * cnt7;
                       }
                       else if(strOP == "/"){
                          lBtm = cnt6 / cnt7;
                       }
                    }

                    lElm = 3;
                    if(lElm == 3){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath = cnt3 + lTop;
                       }
                       else if(strOP == "-"){
                          cmboMath = cnt3 + lTop;
                       }
                       else if(strOP == "*"){
                          cmboMath = cnt3 + lTop;
                       }
                       else if(strOP == "/"){
                          cmboMath = cnt3 + lTop;
                       }
                    }

                    lElm = 9;
                    if(lElm == 9){

                       strOP = localData[lElm];
                       if(strOP == "+"){
                          cmboMath += lBtm;
                       }
                       else if(strOP == "-"){
                          cmboMath += lBtm;
                       }
                       else if(strOP == "*"){
                          cmboMath += lBtm;
                       }
                       else if(strOP == "/"){
                          cmboMath += lBtm;
                       }
                    }
                 } /* end else if((lCnt == 4) && (lCnt2 == 8) &&
                                  (openPar[1] == 10)) */

              } // end if(openPar.size() > 1){

           // 1 () in string
              else{


                     lCnt = openPar[0];
                     lCnt2 = closePar[0];

                  // Means (num op num op num op num) op num
                     if((lCnt == 2) && (lCnt2 == 10)){

                        lElm = 4;
                        if(lElm == 4){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              cmboMath = cnt3 + cnt4;
                           }
                           else if(strOP == "-"){
                              cmboMath = cnt3 - cnt4;
                           }
                           else if(strOP == "*"){
                              cmboMath = cnt3 * cnt4;
                           }
                           else if(strOP == "/"){
                              cmboMath = cnt3 / cnt4;
                           }
                        }

                        lElm = 6;
                        if(lElm == 6){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              cmboMath += cnt5;
                           }
                           else if(strOP == "-"){
                              cmboMath -= cnt5;
                           }
                           else if(strOP == "*"){
                              cmboMath *= cnt5;
                           }
                           else if(strOP == "/"){
                              cmboMath /= cnt5;
                           }
                        }

                        lElm = 8;
                        if(lElm == 8){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              cmboMath += cnt6;
                           }
                           else if(strOP == "-"){
                              cmboMath -= cnt6;
                           }
                           else if(strOP == "*"){
                              cmboMath *= cnt6;
                           }
                           else if(strOP == "/"){
                              cmboMath /= cnt6;
                           }
                        }

                        lElm = 11;
                        if(lElm == 11){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              cmboMath += cnt7;
                           }
                           else if(strOP == "-"){
                              cmboMath -= cnt7;
                           }
                           else if(strOP == "*"){
                              cmboMath *= cnt7;
                           }
                           else if(strOP == "/"){
                              cmboMath /= cnt7;
                           }
                        }
                     } // end if((lCnt == 2) && (lCnt2 == 10))

                  // Means (num op num op num) op num op num
                     else if((lCnt == 2) && (lCnt2 == 8)){

                        lElm = 4;
                        if(lElm == 4){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              lTop = cnt3 + cnt4;
                           }
                           else if(strOP == "-"){
                              lTop = cnt3 - cnt4;
                           }
                           else if(strOP == "*"){
                              lTop = cnt3 * cnt4;
                           }
                           else if(strOP == "/"){
                              lTop = cnt3 / cnt4;
                           }
                        }

                        lElm = 6;
                        if(lElm == 6){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              lTop += cnt5;
                           }
                           else if(strOP == "-"){
                              lTop -= cnt5;
                           }
                           else if(strOP == "*"){
                              lTop *= cnt5;
                           }
                           else if(strOP == "/"){
                              lTop /= cnt5;
                           }
                        }

                        cmboMath = lTop;

                        lElm = 8;
                        if(lElm == 8){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              cmboMath += cnt6;
                           }
                           else if(strOP == "-"){
                              cmboMath -= cnt6;
                           }
                           else if(strOP == "*"){
                              cmboMath *= cnt6;
                           }
                           else if(strOP == "/"){
                              cmboMath /= cnt6;
                           }
                        }

                        lElm = 11;
                        if(lElm == 11){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              cmboMath += cnt7;
                           }
                           else if(strOP == "-"){
                              cmboMath -= cnt7;
                           }
                           else if(strOP == "*"){
                              cmboMath *= cnt7;
                           }
                           else if(strOP == "/"){
                              cmboMath /= cnt7;
                           }
                        }

                     } // end else if((lCnt == 2) && (lCnt2 == 8))

                  // Means (num op num) op num op num op num
                     else if((lCnt == 2) && (lCnt2 == 6)){

                        lElm = 4;
                        if(lElm == 4){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              lTop = cnt3 + cnt4;
                           }
                           else if(strOP == "-"){
                              lTop = cnt3 - cnt4;
                           }
                           else if(strOP == "*"){
                              lTop = cnt3 * cnt4;
                           }
                           else if(strOP == "/"){
                              lTop = cnt3 / cnt4;
                           }
                        }

                        cmboMath = lTop;

                        lElm = 6;
                        if(lElm == 6){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              cmboMath += cnt5;
                           }
                           else if(strOP == "-"){
                              cmboMath -= cnt5;
                           }
                           else if(strOP == "*"){
                              cmboMath *= cnt5;
                           }
                           else if(strOP == "/"){
                              cmboMath /= cnt5;
                           }
                        }

                        lElm = 8;
                        if(lElm == 8){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              cmboMath += cnt6;
                           }
                           else if(strOP == "-"){
                              cmboMath -= cnt6;
                           }
                           else if(strOP == "*"){
                              cmboMath *= cnt6;
                           }
                           else if(strOP == "/"){
                              cmboMath /= cnt6;
                           }
                        }

                        lElm = 11;
                        if(lElm == 11){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              cmboMath += cnt7;
                           }
                           else if(strOP == "-"){
                              cmboMath -= cnt7;
                           }
                           else if(strOP == "*"){
                              cmboMath *= cnt7;
                           }
                           else if(strOP == "/"){
                              cmboMath /= cnt7;
                           }
                        }

                     } // end else if((lCnt == 2) && (lCnt2 == 6))

                  // Means num op (num op num op num op num)
                     else if((lCnt == 4) && (lCnt2 == 12)){

                        if(cnt3 == 0){
                           str1 = localData[2];
                           istringstream (str1) >> cnt3;
                        }

                        lElm = 6;
                        if(lElm == 6){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              lBtm = cnt4 + cnt5;
                           }
                           else if(strOP == "-"){
                              lBtm = cnt4 - cnt5;
                           }
                           else if(strOP == "*"){
                              lBtm = cnt4 * cnt5;
                           }
                           else if(strOP == "/"){
                              lBtm = cnt4 / cnt5;
                           }
                        }

                        lElm = 8;
                        if(lElm == 8){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              lBtm += cnt6;
                           }
                           else if(strOP == "-"){
                              lBtm -= cnt6;
                           }
                           else if(strOP == "*"){
                              lBtm *= cnt6;
                           }
                           else if(strOP == "/"){
                              lBtm /= cnt6;
                           }
                        }

                        lElm = 10;
                        if(lElm == 10){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              lBtm += cnt7;
                           }
                           else if(strOP == "-"){
                              lBtm -= cnt7;
                           }
                           else if(strOP == "*"){
                              lBtm *= cnt7;
                           }
                           else if(strOP == "/"){
                              lBtm /= cnt7;
                           }
                        }

                        lElm = 3;
                        if(lElm == 3){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              lBtm += cnt7;
                           }
                           else if(strOP == "-"){
                              lBtm -= cnt7;
                           }
                           else if(strOP == "*"){
                              lBtm *= cnt7;
                           }
                           else if(strOP == "/"){
                              lBtm /= cnt7;
                           }
                        }

                     }

                  // Means num op num op (num op num op num)
                     else if((lCnt == 6) && (lCnt2 == 12)){

                        if(cnt3 == 0){
                           str1 = localData[2];
                           istringstream (str1) >> cnt3;
                        }
                        if(cnt4 == 0){
                           str1 = localData[4];
                           istringstream (str1) >> cnt4;
                        }

                        lElm = 8;
                        if(lElm == 8){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              lBtm = cnt5 + cnt6;
                           }
                           else if(strOP == "-"){
                              lBtm = cnt5 - cnt6;
                           }
                           else if(strOP == "*"){
                              lBtm = cnt5 * cnt6;
                           }
                           else if(strOP == "/"){
                              lBtm = cnt5 / cnt6;
                           }
                        }

                        lElm = 10;
                        if(lElm == 10){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              lBtm += cnt7;
                           }
                           else if(strOP == "-"){
                              lBtm += cnt7;
                           }
                           else if(strOP == "*"){
                              lBtm += cnt7;
                           }
                           else if(strOP == "/"){
                              lBtm += cnt7;
                           }
                        }

                        lElm = 3;
                        if(lElm == 3){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              lTop = cnt3 + cnt4;
                           }
                           else if(strOP == "-"){
                              lTop = cnt3 - cnt4;
                           }
                           else if(strOP == "*"){
                              lTop = cnt3 * cnt4;
                           }
                           else if(strOP == "/"){
                              lTop = cnt3 / cnt4;
                           }
                        }

                        lElm = 5;
                        if(lElm == 5){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              cmboMath = lTop + lBtm;
                           }
                           else if(strOP == "-"){
                              cmboMath = lTop - lBtm;
                           }
                           else if(strOP == "*"){
                              cmboMath = lTop * lBtm;
                           }
                           else if(strOP == "/"){
                              cmboMath = lTop / lBtm;
                           }
                        }
                     } // end else if((lCnt == 6) && (lCnt2 == 12))

                  // Means num op num op num op (num op num)
                     else if((lCnt == 8) && (lCnt2 == 12)){

                        if(cnt3 == 0){
                           str1 = localData[2];
                           istringstream (str1) >> cnt3;
                        }

                        if(cnt4 == 0){
                           str1 = localData[4];
                           istringstream (str1) >> cnt4;
                        }

                        if(cnt5 == 0){
                           str1 = localData[6];
                           istringstream (str1) >> cnt5;
                        }

                        lElm = 10;
                        if(lElm == 10){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              lBtm = cnt6 + cnt7;
                           }
                           else if(strOP == "-"){
                              lBtm = cnt6 - cnt7;
                           }
                           else if(strOP == "*"){
                              lBtm = cnt6 * cnt7;
                           }
                           else if(strOP == "/"){
                              lBtm = cnt6 / cnt7;
                           }
                        }

                        lElm = 3;
                        if(lElm == 3){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              lTop = cnt3 + cnt4;
                           }
                           else if(strOP == "-"){
                              lTop = cnt3 - cnt4;
                           }
                           else if(strOP == "*"){
                              lTop = cnt3 * cnt4;
                           }
                           else if(strOP == "/"){
                              lTop = cnt3 / cnt4;
                           }
                        }

                        lElm = 5;
                        if(lElm == 5){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              lTop += cnt5;
                           }
                           else if(strOP == "-"){
                              lTop -= cnt5;
                           }
                           else if(strOP == "*"){
                              lTop *= cnt5;
                           }
                           else if(strOP == "/"){
                              lTop /= cnt5;
                           }
                        }

                        lElm = 7;
                        if(lElm == 7){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              cmboMath = lTop + lBtm;
                           }
                           else if(strOP == "-"){
                              cmboMath = lTop - lBtm;
                           }
                           else if(strOP == "*"){
                              cmboMath = lTop * lBtm;
                           }
                           else if(strOP == "/"){
                              cmboMath = lTop / lBtm;
                           }
                        }
                     } // end else if((lCnt == 8) && (lCnt2 == 12))

                  // Means num op (num op num op num) op num
                     else if((lCnt == 4) && (lCnt2 == 10)){

                        if(cnt3 == 0){
                           str1 = localData[2];
                           istringstream (str1) >> cnt3;
                        }

                        lTop = cnt3;

                        lElm = 6;
                        if(lElm == 6){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              lBtm = cnt4 + cnt5;
                           }
                           else if(strOP == "-"){
                              lBtm = cnt4 - cnt5;
                           }
                           else if(strOP == "*"){
                              lBtm = cnt4 * cnt5;
                           }
                           else if(strOP == "/"){
                              lBtm = cnt4 / cnt5;
                           }
                        }

                        lElm = 8;
                        if(lElm == 8){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              lBtm += cnt6;
                           }
                           else if(strOP == "-"){
                              lBtm -= cnt6;
                           }
                           else if(strOP == "*"){
                              lBtm *= cnt6;
                           }
                           else if(strOP == "/"){
                              lBtm /= cnt6;
                           }
                        }

                        lElm = 3;
                        if(lElm == 3){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              cmboMath = lTop + lBtm;
                           }
                           else if(strOP == "-"){
                              cmboMath = lTop - lBtm;
                           }
                           else if(strOP == "*"){
                              cmboMath = lTop * lBtm;
                           }
                           else if(strOP == "/"){
                              cmboMath = lTop / lBtm;
                           }
                        }

                        lElm = 11;
                        if(lElm == 11){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              cmboMath += cnt7;
                           }
                           else if(strOP == "-"){
                              cmboMath -= cnt7;
                           }
                           else if(strOP == "*"){
                              cmboMath *= cnt7;
                           }
                           else if(strOP == "/"){
                              cmboMath /= cnt7;
                           }
                        }

                     } // end else if((lCnt == 4) && (lCnt2 == 10))

                  // Means num op (num op num) op num op num
                     else if((lCnt == 4) && (lCnt2 == 8)){

                        if(cnt3 == 0){
                           str1 = localData[2];
                           istringstream (str1) >> cnt3;
                        }

                        lTop = cnt3;

                        lElm = 6;
                        if(lElm == 6){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              lBtm = cnt4 + cnt5;
                           }
                           else if(strOP == "-"){
                              lBtm = cnt4 - cnt5;
                           }
                           else if(strOP == "*"){
                              lBtm = cnt4 * cnt5;
                           }
                           else if(strOP == "/"){
                              lBtm = cnt4 / cnt5;
                           }
                        }

                        lElm = 3;
                        if(lElm == 3){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              cmboMath = lTop + lBtm;
                           }
                           else if(strOP == "-"){
                              cmboMath = lTop + lBtm;
                           }
                           else if(strOP == "*"){
                              cmboMath = lTop + lBtm;
                           }
                           else if(strOP == "/"){
                              cmboMath = lTop + lBtm;
                           }
                        }

                        lElm = 9;
                        if(lElm == 9){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              cmboMath += cnt6;
                           }
                           else if(strOP == "-"){
                              cmboMath -= cnt6;
                           }
                           else if(strOP == "*"){
                              cmboMath *= cnt6;
                           }
                           else if(strOP == "/"){
                              cmboMath /= cnt6;
                           }
                        }

                        lElm = 11;
                        if(lElm == 11){

                           strOP = localData[lElm];
                           if(strOP == "+"){
                              cmboMath += cnt7;
                           }
                           else if(strOP == "-"){
                              cmboMath -= cnt7;
                           }
                           else if(strOP == "*"){
                              cmboMath *= cnt7;
                           }
                           else if(strOP == "/"){
                              cmboMath /= cnt7;
                           }
                        }

                     } // end else if((lCnt == 4) && (lCnt2 == 8))
               } //       end else (1 () in string)

         } // end if(lStep == 4)

         return cmboMath;
	}

	int math_NonParentheses(vector <string> &localData, int lStep, int lElm)
	{
	     string strOP = "";
         int cmboMath = 0;

         if(lStep == 2){

            lElm = 3;
            if(lElm == 3){

               strOP = localData[lElm];
               if(strOP == "+"){
                   cmboMath = cnt3 + cnt4;
               }
               else if(strOP == "-"){
                   cmboMath = cnt3 - cnt4;
               }
               else if(strOP == "*"){
                   cmboMath = cnt3 * cnt4;
               }
               else if(strOP == "/"){
                   cmboMath = cnt3 / cnt4;
               }
            }

            lElm = 5;
            if(lElm == 5){

               strOP = localData[lElm];
               if(strOP == "+"){
                   cmboMath += cnt5;
               }
               else if(strOP == "-"){
                   cmboMath -= cnt5;
               }
               else if(strOP == "*"){
                   cmboMath *= cnt5;
               }
               else if(strOP == "/"){
                   cmboMath /= cnt5;
               }
            }
         }
         else if(lStep > 2){

            strOP = localData[lElm];
            if(strOP == "+"){
               cmboMath = cnt3 + cnt4;
            }
            else if(strOP == "-"){
               cmboMath = cnt3 - cnt4;
            }
            else if(strOP == "*"){
               cmboMath = cnt3 * cnt4;
            }
            else{
               cmboMath = cnt3 / cnt4;
            }

            strOP = localData[lElm + 2];
            if(strOP == "+"){
               cmboMath += cnt5;
            }
            else if(strOP == "-"){
               cmboMath -= cnt5;
            }
            else if(strOP == "*"){
               cmboMath *= cnt5;
            }
            else{
               cmboMath /= cnt5;
            }

            strOP = localData[lElm + 4];
            if(strOP == "+"){
               cmboMath += cnt6;
            }
            else if(strOP == "-"){
               cmboMath -= cnt6;
            }
            else if(strOP == "*"){
               cmboMath *= cnt6;
            }
            else{
               cmboMath /= cnt6;
            }

            if(lStep == 4){

               strOP = localData[lElm + 6];
               if(strOP == "+"){
                  cmboMath += cnt7;
               }
               else if(strOP == "-"){
                  cmboMath -= cnt7;
               }
               else if(strOP == "*"){
                  cmboMath *= cnt7;
               }
               else{
                  cmboMath /= cnt7;
               }

            }
         }

         return cmboMath;
	}

	void modify()
	{
	   bool ck = false;
	  // bool ck1 = false;

       int lCnt, lCnt2, lElm, lElm2, lBeg, lSecEnd;
       lCnt = lCnt2 = lElm = lElm2 = lBeg = lSecEnd = 0;
       //int ActElm = 0;

       int iAnswer, lOpt1, lOpt2, lOpt3;
       iAnswer = lOpt1 = lOpt2 = lOpt3 = 0;

       string sKeys, sBrkt, sKData, sAnswer, str1, str2, str3;
       sKeys = sBrkt = sKData = sAnswer = str1 = str2 = str3 = "";

    // clearing out old data
       if(SectModData.size() > 0){ SectModData.clear(); }
       if(dta.size() > 0){ dta.clear(); }

       UpdatAssoAry = true;

    /* this while section will display the question
       about selecting between creating a new section
       or modifying an existing section */

       while(!ck){

         titleScreen(7);

         cout<<"Enter here => ";
         getline(cin, sAnswer);

      // making sure a number was entered
         istringstream (sAnswer) >> iAnswer;
         sprintf(ConvNum, "%d", iAnswer);
         str1 = ConvNum;

      // Testing for numbers entered
         if((str1 == sAnswer) && (str1 != "0")){

            --iAnswer;
            if(iAnswer <= 2 ){

            // Create new section
               if(iAnswer == 0){
                  cnt7 = lOpt1 = 0;
                  line = "New Section";
               }

            // add to existing
               else{
                  cnt7 = lOpt1 = 1;
                  line = "Existing Section";
               }

               ck = true;
            }
         }// end if((str1 == sAnswer) && (str1 != "0"))
       }//   end while(!ck)

       ck = false;

       while(!ck){

           titleScreen(8);

           cout<<"Enter here => ";
           getline(cin, sAnswer);

        // making sure a number was entered
           istringstream (sAnswer) >> iAnswer;
           sprintf(ConvNum, "%d", iAnswer);
           str1 = ConvNum;

           if((str1 == sAnswer) && (str1 != "0")){

              lCnt = jsDataMod.size();
              if(iAnswer <= lCnt ){
                 lOpt2 = iAnswer;
                 ck = true;
              }
           }
       }// end while(!ck)

       ck = false;

       while(!ck){

           titleScreen(9);

           cout<<"Enter here => ";
           getline(cin, sAnswer);

        // making sure a number was entered
           istringstream (sAnswer) >> iAnswer;
           sprintf(ConvNum, "%d", iAnswer);
           str1 = ConvNum;

           if((str1 == sAnswer) && (str1 != "0")){

              lCnt = jsDataMod.size();
              if(iAnswer <= lCnt ){
                 lOpt3 = iAnswer;

                 if(lOpt1 == 0){
                    modify();
                 }
                 else{
                    modifyExisting(lOpt3, lOpt2);
                 }
                 ck = true;
              }
           }
       }// end while(!ck)


    // updating and writing to the file

       if(UpdatAssoAry){
         updateAssoAry();
         updateNoKeyData();
       }
       else{ UpdatAssoAry = true; }

       BeforeWriteCheck();
       WriteFile();

    // showing main screen
       titleScreen(1);

	}

	void modifyNew()
	{

	}

	void modifyExisting(int &opt, int &ActElm)
	{
	    bool ck1 = false;
	    bool ck2 = false;

	    string str1, str2, str3, str4, str5, sAnswer;
	    str1 = str2 = str3 = str4 = str5 = sAnswer = "";

	    vector <string> localData;
	    vector<vector <string>> localData2;


	    int lCnt, lCnt2, lElm, lSize, lSize2, iAnswer;
	    lCnt = lCnt2 = lElm = lSize = lSize2 = iAnswer = 0;

	    if(dta.size() > 0){ dta.clear(); }
	    dta.resize(2, "");

	    if(cnt7 == 1){
	        switch(opt){
	           case 1:

	          /**********************
	           * Adding key/keydata *
	           **********************/

	             while(!ck1){
                    cout<<"\n\n***********"<<endl;
                    cout<<" Keyword  *"<<endl;
                    cout<<"***********"<<endl;
                    cout<<"\nEnter keyword => ";
                    getline(cin, sAnswer);

                    cout<<"\nyou entered [ "<<sAnswer<<" ]"<<endl;
                    cout<<"Correct... y or n => ";
                    getline(cin, str1);

                    if((str1 == "Y") || (str1 == "y")){

                       str1 = "\"" + sAnswer + "\"";
                       lCnt = spaceCnt[ActElm-1];
                       str2 = EndComma[ActElm-1];
                       str4 = KeyBrkt[ActElm-1];

                       if((str2 == "") || (str2 == " ")){
                          str3 = EndComma[ActElm-2];
                          EndComma[ActElm-1] = str3;
                          str4 = KeyBrkt[ActElm-1];

                          keys.insert(keys.begin() + ActElm, str1);
                          KeyBrkt.insert(KeyBrkt.begin() + ActElm, str4);

                          spaceCnt.insert(spaceCnt.begin() + ActElm, lCnt);
                          EndComma.insert(EndComma.begin() + ActElm, str2);
                       }
                       else{
                          keys.insert(keys.begin() + ActElm, str1);
                          KeyBrkt.insert(KeyBrkt.begin() + ActElm, str4);
                          spaceCnt.insert(spaceCnt.begin() + ActElm, lCnt);
                          EndComma.insert(EndComma.begin() + ActElm, str2);
                       }

                       jsDataMod.push_back(dta);

                       lCnt = jsDataMod.size();
                       for(int i=lCnt-1; i>ActElm; i--){
                          lCnt2 = i-1;

                          str3 = jsDataMod[lCnt2][0];
                          jsDataMod[i][0] = str3;
                       }

                       jsDataMod[ActElm][0] = str1;

                       ck1 = true;

                    }// end if((str1 == "Y") || (str1 == "y"))
                 }//    end while(!ck1)

                 ck1 = false;


                 while(!ck1){

                    cout<<"\n************"<<endl;
                    cout<<" Key Data  *"<<endl;
                    cout<<"************"<<endl;

                    cout<<"\nEnter key data => ";
                    getline(cin, sAnswer);
                    cout<<"you entered [ "<<sAnswer<<" ]"<<endl;

                    cout<<"\nCorrect... y or n => ";
                    getline(cin, str3);

                    if((str3 == "Y") || (str3 == "y" )){

                       str1 = "\"" + sAnswer + "\"";
                       keyData.insert(keyData.begin() + ActElm, str1);
                       ck1 = true;
                    }
                 }

	             break;

	           case 2:

	          /**********************
	           * Adding key/AssoAry *
	           **********************/

	             cout<<"\n\n***************"<<endl;
	             cout<<"* key/AssoAry *"<<endl;
	             cout<<"***************\n"<<endl;

	             cout<<"1) Create new key/Array pair"<<endl;
	             cout<<"2) Add a new element to array\n"<<endl;

	             ck1 = false;
	             while(!ck1){

                    cout<<"Enter here => ";
                    getline(cin, sAnswer);

                 // making sure a number was entered
                    istringstream (sAnswer) >> iAnswer;
                    sprintf(ConvNum, "%d", iAnswer);
                    str1 = ConvNum;
                    --ActElm;
                    str2 = keyData[ActElm];

                    if((str1 == sAnswer) && (str1 != "0")){

                        if(iAnswer == 1){

                         /*---------------------------------
                            Creating a new key/assoAry pair
                           --------------------------------- */

                           //if(dta.size() > 0){ dta.clear(); }

                           str3 = "";
                           for(unsigned int i=0; i<assoKeyAry.size(); i++){

                              lCnt2 = assoKeyAry[i].size();
                              for(int j=0; j<lCnt2; j++){

		                         if((j==0) || (j == lCnt2-1)){
		                           str3 += assoKeyAry[i][j];
		                         }
		                         else if(j == lCnt2-2){
		                           str3 += assoKeyAry[i][j];
		                         }
		                         else{
		                           str3 += assoKeyAry[i][j];
		                           str3 += ", ";
		                         }
                              }
                              if(str2 == str3){
                                 lElm = i;
                                 break;
                              }
                              str3 = "";
                           }

                           while(!ck2){

                              cout<<"\n\n***********************************"<<endl;
                              cout<<"* Creating a new key/assoAry pair *"<<endl;
                              cout<<"***********************************\n"<<endl;

                              cout<<"Enter the keyword => ";
                              getline(cin, sAnswer);
                              str1 = sAnswer;

                              cout<<"You entered [ "<<sAnswer<<" ]"<<endl;
                              cout<<"correct... y or n => ";
                              getline(cin, sAnswer);

                              if((sAnswer == "Y") || (sAnswer == "y")){
                                 ck2 = true;
                              }
                           }

                           str4 = EndComma[ActElm];
                           lCnt = spaceCnt[ActElm-1];

                           if((str4 == "") || (str4 == " ")){
                              str3 = EndComma[ActElm-1];
                              EndComma[ActElm] = str3;
                              EndComma.insert(EndComma.begin() + ActElm-1, str4);
                              str4 = KeyBrkt[ActElm-1];

                              str1 = "\"" + str1 + "\"";
                              keys.insert(keys.begin() + ActElm, str1);
                              KeyBrkt.insert(KeyBrkt.begin() + ActElm, str4);

                              spaceCnt.insert(spaceCnt.begin() + ActElm, lCnt);
                           }
                           else{

                              str1 = "\"" + str1 + "\"";
                              keys.insert(keys.begin() + ActElm, str1);

                              str3 = KeyBrkt[ActElm-1];
                              KeyBrkt.insert(KeyBrkt.begin() + ActElm, str3);

                              spaceCnt.insert(spaceCnt.begin() + ActElm, lCnt);
                              EndComma.insert(EndComma.begin() + ActElm-1, str4);
                           }

                           jsDataMod.push_back(dta);
                           if(dta.size() > 0){ dta.clear(); }
                           ck2 = false;

                           cout<<"\n\n*********************************** "<<endl;
                           cout<<"* Creating a new key/assoAry pair * "<<endl;
                           cout<<"*********************************** "<<endl;
                           cout<<"\nType done when you're finished    "<<endl;

                           dta.push_back("[");

                           while(!ck2){

                               cout<<"Enter your array item => ";
                               getline(cin, sAnswer);

                               if(sAnswer != "done"){
                                  str4 = "\"" + sAnswer + "\"";
                                  dta.push_back(str4);
                               }
                               else{
                                  dta.push_back("]");
                                  ck2 = true;
                               }
                           }

                           assoKeyAry.insert(assoKeyAry.begin() + lElm, dta);

                           if(dta.size() > 0){ dta.clear(); }
                           str3 = "";

                           for(int i=0; i<lCnt2; i++){

		                      if((i==0) || (i == lCnt2-1)){
		                        str3 += dta[i];
		                      }
		                      else if(i == lCnt2-2){
		                        str3 += dta[i];
		                      }
		                      else{
		                        str3 += dta[i];
		                        str3 += ", ";
		                      }
                           }

                           keyData.insert(keyData.begin() + ActElm, str3);

                           if(dta.size() > 0){ dta.clear(); }
                           //dta.resize(3, "");
                           //jsDataMod.push_back(dta);

                           lCnt= jsDataMod.size()-1;
                           for(int i=lCnt; i>ActElm; i--){

                              lCnt2 = i-1;
                              str3 = jsDataMod[lCnt2][0];
                              jsDataMod[i][0] = str3;
                           }

                           jsDataMod[ActElm][0] = str1;
                           ck1 = true;
                           UpdatAssoAry = false;

                           if(localData2.size() > 0){ localData2.clear(); }
                           if(dta.size() > 0){ dta.clear(); }
                        }
                        else if(iAnswer == 2){

                         /*---------------------------------
                            Adding a new element to array
                           --------------------------------- */

                         /* building a string to be used for comparison
                            to choose the correct array */
                            str3 = "";
                            for(unsigned int i=0; i<assoKeyAry.size(); i++){

                               lCnt2 = assoKeyAry[i].size();
                               for(int j=0; j<lCnt2; j++){

		                          if((j==0) || (j == lCnt2-1)){
		                            str3 += assoKeyAry[i][j];
		                          }
		                          else if(j == lCnt2-2){
		                            str3 += assoKeyAry[i][j];
		                          }
		                          else{
		                            str3 += assoKeyAry[i][j];
		                            str3 += ", ";
		                          }
                               }
                               if(str2 == str3){
                                  lElm = i;
                                  break;
                               }
                               str3 = "";
                            }

                            lCnt = assoKeyAry[lElm].size();
                            cout<<"\n";
                            cout<<"Current elements are"<<endl;
                            cout<<"----------------------"<<endl;

                            for(int i=1; i<lCnt; i++){

                               if(i<lCnt-1){
                                  cout<<"=> "<<assoKeyAry[lElm][i]<<endl;
                               }
                            }

                            while(!ck2){

                               cout<<"\n\nEnter the data for\nthe new element => ";
                               getline(cin, sAnswer);

                               cout<<"\n\nYou entered [ "<<sAnswer<<" ]"<<endl;
                               cout<<"correct... y or n => ";

                               getline(cin, str3);
                               if((str3 == "Y") || (str3 == "y")){

                                  str4 = "\"" + sAnswer + "\"";
                                  lCnt2 = assoKeyAry[lElm].size();

                                  assoKeyAry[lElm].push_back("");
                                  lCnt2 = assoKeyAry[lElm].size();

                               /* shifting the "]" to the right and storing the new data */
                                  assoKeyAry[lElm][lCnt2-1] = assoKeyAry[lElm][lCnt2-2];
                                  assoKeyAry[lElm][lCnt2-2] = str4;


                               // rebuilding the string to be stored
                                  for(int i=0; i<lCnt2; i++){

		                              if((i==0) || (i == lCnt2-1)){
		                                str5 += assoKeyAry[lElm][i];
		                              }
		                              else if(i == lCnt2-2){
		                                str5 += assoKeyAry[lElm][i];
		                              }
		                              else{
		                                str5 += assoKeyAry[lElm][i];
		                                str5 += ", ";
		                              }

                                      keyData[ActElm] = str5;
                                      ck2 = true;
                                      ck1 = true;

                                  }// end for(int i=0; i<lCnt2; i++)
                                }//   end if((str3 == "Y") || (str3 == "y"))
                            }//       end while(!ck2)

                        }//     end else if(iAnswer == 2)
                    }//         end if((str1 == sAnswer) && (str1 != "0"))
                 }//            end while(!ck1)

	             break;

               case 3:

	          /**********************
	           * Adding No Key Array *
	           **********************/

	             cout<<"\n\n****************"<<endl;
	             cout<<"* No Key Array *"<<endl;
	             cout<<"****************\n"<<endl;

/*
	             cout<<"1) Create new key/Array pair"<<endl;
*/
	             cout<<"2) Add a new element to array\n"<<endl;

	             ck1 = false;
	             while(!ck1){

                    cout<<"Enter here => ";
                    getline(cin, sAnswer);

                 // making sure a number was entered
                    istringstream (sAnswer) >> iAnswer;
                    sprintf(ConvNum, "%d", iAnswer);
                    str1 = ConvNum;
                    --ActElm;

                    str2 = jsDataMod[ActElm][0];
                    str3 = str2[str2.size()-1];

                    if(str3 == ","){
                       str2.erase(str2.begin() + str2.size()-1);
                    }


                    if((str1 == sAnswer) && (str1 != "0")){

                        if(iAnswer == 1){

                        }
                        else if(iAnswer == 2){

                         /*---------------------------------
                            Adding a new element to array
                           --------------------------------- */

                         /* building a string to be used for comparison
                            to choose the correct array */
                            str3 = "";
                            for(unsigned int i=0; i<noKeyAry.size(); i++){

                               lCnt2 = noKeyAry[i].size();
                               for(int j=0; j<lCnt2; j++){

		                          if((j==0) || (j == lCnt2-1)){
		                            str3 += "[";
		                            str3 += noKeyAry[i][j];
		                            str3 += ", ";
		                          }
		                          else if(j == lCnt2-2){
		                            str3 += noKeyAry[i][j];
		                            str3 += "]";
		                            break;
		                          }
		                          else{
		                            str3 += noKeyAry[i][j];
		                            str3 += ", ";
		                          }
                               }
                               if(str2 == str3){
                                  lElm = i;
                                  break;
                               }
                               str3 = "";
                            }

                            lCnt = noKeyAry[lElm].size();
                            cout<<"\n";
                            cout<<"Current elements are"<<endl;
                            cout<<"----------------------"<<endl;

                            for(int i=1; i<lCnt; i++){

                               if(i<lCnt-1){
                                  cout<<"=> "<<noKeyAry[lElm][i]<<endl;
                               }
                            }

                            while(!ck2){

                               cout<<"\n\nEnter the data for\nthe new element => ";
                               getline(cin, sAnswer);

                               cout<<"\n\nYou entered [ "<<sAnswer<<" ]"<<endl;
                               cout<<"correct... y or n => ";

                               getline(cin, str3);
                               if((str3 == "Y") || (str3 == "y")){

                                  str4 = "\"" + sAnswer + "\"";
                                  lCnt2 = noKeyAry[lElm].size();

                                  noKeyAry[lElm].push_back("");
                                  lCnt2 = noKeyAry[lElm].size();

                               /* shifting the "]" to the right and storing the new data */
                                  noKeyAry[lElm][lCnt2-1] = noKeyAry[lElm][lCnt2-2];
                                  noKeyAry[lElm][lCnt2-2] = str4;


                               // rebuilding the string to be stored
                                  for(int i=0; i<lCnt2; i++){

		                              if((i==0) || (i == lCnt2-1)){
		                                str5 += noKeyAry[lElm][i];
		                              }
		                              else if(i == lCnt2-2){
		                                str5 += noKeyAry[lElm][i];
		                              }
		                              else{
		                                str5 += noKeyAry[lElm][i];
		                                str5 += ", ";
		                              }

                                      keyData[ActElm] = str5;
                                      ck2 = true;
                                      ck1 = true;

                                  }// end for(int i=0; i<lCnt2; i++)
                                }//   end if((str3 == "Y") || (str3 == "y"))
                            }//       end while(!ck2)

                        }//     end else if(iAnswer == 2)
                    }//         end if((str1 == sAnswer) && (str1 != "0"))
                 }//            end while(!ck1)
                 break;

	           case 4:

	          /************************
	           * Deleting key/keydata *
	           ************************/

	             --ActElm;
	             keys.erase(keys.begin() + ActElm);
	             keyData.erase(keyData.begin() + ActElm);
	             KeyBrkt.erase(KeyBrkt.begin() + ActElm);
	             spaceCnt.erase(spaceCnt.begin() + ActElm);

              /* getting the current element
                 data to be used after removal */

	             str1 = EndComma[ActElm];
	             EndComma.erase(EndComma.begin() + ActElm);
	             EndComma[ActElm-1] = str1;

	             lCnt = jsDataMod.size();

	             for(int i=0; i<lCnt; i++){
	                if(i != ActElm){
	                  localData.push_back(jsDataMod[i][0]);
	                }
	             }

	             jsDataMod.pop_back();
	             lCnt = jsDataMod.size();

	             for(int i=0; i<lCnt; i++){
	                jsDataMod[i][0] = localData[i];
	             }

	             break;

	           case 5:

	          /************************
	           * Deleting key/Array   *
	           ************************/

                 if(dta.size() > 0){ dta.clear(); }

	             cout<<"\n\n***************"<<endl;
	             cout<<"* key/AssoAry *"<<endl;
	             cout<<"***************\n"<<endl;
/*
	             cout<<"1) Delete key/Array pair"<<endl;
*/
	             cout<<"2) Delete an element in the array\n"<<endl;

	             ck1 = false;
	             while(!ck1){

                    cout<<"Enter here => ";
                    getline(cin, sAnswer);

                 // making sure a number was entered
                    istringstream (sAnswer) >> iAnswer;
                    sprintf(ConvNum, "%d", iAnswer);
                    str1 = ConvNum;
                    --ActElm;
                    str2 = keyData[ActElm];

                    str3 = str2[str2.size()-1];

                    if(str3 == ","){
                       str2.erase(str2.begin() + str2.size()-1);
                    }


                    if((str1 == sAnswer) && (str1 != "0")){

                        if(iAnswer == 1){

                           // Deleting an key/assoAry pair

                        }
                        else if(iAnswer == 2){

                         /*---------------------------------
                            Deleting an element in the array
                           --------------------------------- */

                         /* building a string to be used for comparison
                            to choose the correct array */
                            str3 = "";
                            for(unsigned int i=0; i<assoKeyAry.size(); i++){

                               lCnt2 = assoKeyAry[i].size();
                               for(int j=0; j<lCnt2; j++){

		                          if((j==0) || (j == lCnt2-1)){
		                            str3 += assoKeyAry[i][j];
		                          }
		                          else if(j == lCnt2-2){
		                            str3 += assoKeyAry[i][j];
		                          }
		                          else{
		                            str3 += assoKeyAry[i][j];
		                            str3 += ", ";
		                          }
                               }
                               if(str2 == str3){
                                  lElm = i;
                                  break;
                               }
                               str3 = "";
                            }

                            lCnt = assoKeyAry[lElm].size();
                            cout<<"\n";
                            cout<<"Current elements are"<<endl;
                            cout<<"----------------------"<<endl;

                            for(int i=1; i<lCnt; i++){

                               if(i<lCnt-1){
                                  cout<<i<<") "<<assoKeyAry[lElm][i]<<endl;
                               }
                            }

                            while(!ck2){

                               cout<<"\n\nSelect the element you want to remove => ";
                               getline(cin, sAnswer);

                            // making sure a number was entered
                               istringstream (sAnswer) >> iAnswer;
                               sprintf(ConvNum, "%d", iAnswer);
                               str3 = ConvNum;

                               cout<<"\nYou entered [ "<<assoKeyAry[lElm][iAnswer]<<" ]"<<endl;
                               cout<<"\ncorrect... y or n => ";

                               getline(cin, str3);
                               if((str3 == "Y") || (str3 == "y")){

                                  //--iAnswer;
                                  lCnt2 = assoKeyAry[lElm].size();

                                  for(int i=0; i<lCnt2; i++){
                                     if(i != iAnswer){
                                        dta.push_back(assoKeyAry[lElm][i]);
                                     }
                                  }

                                  assoKeyAry[lElm].pop_back();

                               /* refilling the array with
                                  data minus the deleted one */

                                  for(int i=0; i<lCnt2; i++){
                                     assoKeyAry[lElm][i] = dta[i];
                                  }

                               // rebuilding the string to be stored
                                  for(int i=0; i<lCnt2; i++){

		                              if((i==0) || (i == lCnt2-1)){
		                                str5 += assoKeyAry[lElm][i];
		                              }
		                              else if(i == lCnt2-2){
		                                str5 += assoKeyAry[lElm][i];
		                              }
		                              else{
		                                str5 += assoKeyAry[lElm][i];
		                                str5 += ", ";
		                              }
                                  }// end for(int i=0; i<lCnt2; i++)

                                  keyData[ActElm] = str5;
                                  ck2 = true;
                                  ck1 = true;

                                }//   end if((str3 == "Y") || (str3 == "y"))
                            }//       end while(!ck2)

                        }//     end else if(iAnswer == 2)
                    }//         end if((str1 == sAnswer) && (str1 != "0"))
                 }//             while(!ck1)

	             break;

               case 6:


	          /*************************
	           * Deleting key/Array    *
	           *************************/

                 if(dta.size() > 0){ dta.clear(); }

	             cout<<"\n\n****************"<<endl;
	             cout<<"* No Key Array *"<<endl;
	             cout<<"****************\n"<<endl;
/*
	             cout<<"1) Delete key/Array pair"<<endl;
*/
	             cout<<"2) Delete an element in the array\n"<<endl;

	             ck1 = false;
	             while(!ck1){

                    cout<<"Enter here => ";
                    getline(cin, sAnswer);

                 // making sure a number was entered
                    istringstream (sAnswer) >> iAnswer;
                    sprintf(ConvNum, "%d", iAnswer);
                    str1 = ConvNum;
                    --ActElm;
                    str2 = jsDataMod[ActElm][0];
                    str3 = str2[str2.size()-1];

                    if(str3 == ","){
                       str2.erase(str2.begin() + str2.size()-1);
                    }

                    if((str1 == sAnswer) && (str1 != "0")){

                        if(iAnswer == 1){

                           // Creating a new key/assoAry pair

                        }
                        else if(iAnswer == 2){

                         /*---------------------------------
                            Deleting an element in the array
                           --------------------------------- */



                         /* building a string to be used for comparison
                            to choose the correct array */
                            str3 = "";
                            for(unsigned int i=0; i<noKeyAry.size(); i++){

                               lCnt2 = noKeyAry[i].size();
                               for(int j=0; j<lCnt2; j++){

		                          if((j==0) || (j == lCnt2-1)){
		                            str3 += "[";
		                            str3 += noKeyAry[i][j];
		                            str3 += ", ";
		                          }
		                          else if(j == lCnt2-2){
		                            str3 += noKeyAry[i][j];
		                            str3 += "]";
		                            break;
		                          }
		                          else{
		                            str3 += noKeyAry[i][j];
		                            str3 += ", ";
		                          }
                               }
                               if(str2 == str3){
                                  lElm = i;
                                  break;
                               }
                               str3 = "";
                            }

                            lCnt = noKeyAry[lElm].size();
                            cout<<"\n";
                            cout<<"Current elements are"<<endl;
                            cout<<"----------------------"<<endl;

                            for(int i=1; i<lCnt; i++){

                               if(i<lCnt-1){
                                  cout<<i<<") "<<noKeyAry[lElm][i]<<endl;
                               }
                            }

                            while(!ck2){

                               cout<<"\n\nSelect the element you want to remove => ";
                               getline(cin, sAnswer);

                            // making sure a number was entered
                               istringstream (sAnswer) >> iAnswer;
                               sprintf(ConvNum, "%d", iAnswer);
                               str3 = ConvNum;

                               cout<<"\nYou entered [ "<<noKeyAry[lElm][iAnswer]<<" ]"<<endl;
                               cout<<"\ncorrect... y or n => ";

                               getline(cin, str3);
                               if((str3 == "Y") || (str3 == "y")){

                                  //--iAnswer;
                                  lCnt2 = noKeyAry[lElm].size();

                                  for(int i=0; i<lCnt2; i++){
                                     if(i != iAnswer){
                                        dta.push_back(noKeyAry[lElm][i]);
                                     }
                                  }

                                  noKeyAry[lElm].pop_back();

                               /* refilling the array with
                                  data minus the deleted one */

                                  for(int i=0; i<lCnt2; i++){
                                     noKeyAry[lElm][i] = dta[i];
                                  }

                               // rebuilding the string to be stored
                                  for(int i=0; i<lCnt2; i++){

		                              if((i==0) || (i == lCnt2-1)){
		                                str5 += noKeyAry[lElm][i];
		                              }
		                              else if(i == lCnt2-2){
		                                str5 += noKeyAry[lElm][i];
		                              }
		                              else{
		                                str5 += noKeyAry[lElm][i];
		                                str5 += ", ";
		                              }
                                  }// end for(int i=0; i<lCnt2; i++)

                                  keyData[ActElm] = str5;
                                  ck2 = true;
                                  ck1 = true;

                                }//   end if((str3 == "Y") || (str3 == "y"))
                            }//       end while(!ck2)

                        }//     end else if(iAnswer == 2)
                    }//         end if((str1 == sAnswer) && (str1 != "0"))
                 }//            while(!ck1)

                 break;
               default: break;

	        }// end switch(opt)
        }//     end if(cnt7 == 1)

        if(dta.size() > 0){ dta.clear(); }
        if(localData.size() > 0){ localData.clear(); }
	}

	bool MultiBracket()
	{
		bool ck = false;
		int fElm = 0;
		string str1, str2;
		str1 = str2 = "";
		elmCK1 = elmCK2 = elmCK3 = elmCK4 = 0;

     /* seeing how many brackets were in the user typed
        string. it shows how big the multiDem array is */

		cnt5 = bracketID.size();
        cnt = 0;

        switch(cnt5){
			case 2:
			   elmCK1 = bracketID[0];
			   elmCK2 = bracketID[1];
			   elmCK3 = 0;
			   elmCK4 = 0;
			   cnt = 2;
			   break;

			case 3:
			   elmCK1 = bracketID[0];
			   elmCK2 = bracketID[1];
			   elmCK3 = bracketID[2];
			   elmCK4 = 0;
			   cnt = 3;
			   break;

			case 4:
			   elmCK1 = bracketID[0];
			   elmCK2 = bracketID[1];
			   elmCK3 = bracketID[2];
			   elmCK4 = bracketID[3];
			   cnt = 4;
			   break;

			default: break;
		}


     /* selecting the end element value and
        converting it to integer. then use
        it to show the total line. */

        cnt5 = noKeyAry[elmCK1].size()-1;
        str1 = noKeyAry[elmCK1][cnt5];
        istringstream (str1) >> fElm;

		str2 = "Complete line is ";
		str2 += jsDataMod[fElm][0];
		str2 += "\nand the actual place value is ";

		switch(cnt){
		   case 2:
			   str2 += noKeyAry[elmCK1][elmCK2];
			   vStr = noKeyAry[elmCK1][elmCK2];
			   break;
	 	   case 3:
			   str2 += noKeyAry[elmCK1][elmCK2][elmCK3];
			   vStr = noKeyAry[elmCK1][elmCK2][elmCK3];
			   break;
           case 4:
			   //str2 += noKeyAry[elmCK1][elmCK2][elmCK3][elmCK4];
			   //vStr = noKeyAry[elmCK1][elmCK2][elmCK3][elmCK4];
			   break;
           default: break;
		}

	  //Displaying the multi-bracketed data
        cout<<str2<<endl;
        setDataPosition("jsDataMod", fElm);

        ck = true;
        return ck;
	}

	void ReadFile(int selector)
	{
	   switch(selector){

	      case 0:
            myfile.open(FileName, ios::out | ios::in);
            break;
          case 1:
            myfile.open(FileBKup, ios::out | ios::in);
            break;
          case 3:
            myfile.open(FileVariables, ios::out | ios::in);
            varExist = true;
            break;
          case 4:
            myfile.open(FileUserFiles, ios::out | ios::in);
            varExist = false;
            break;
          default: break;
	   }

   /*-------------------------------
       Reading the raw json file
       and storing it in vector
       array
     -------------------------------*/

	   if(myfile.is_open()){

		  while(getline (myfile, line)){
			 jsDataRaw.push_back(line);
		  }

		  CloseFile();
	   }
	   else{
	      //cout<<"Can't open file";
	      if(selector == 3){ varExist = false; }
	      else if(selector == 4){ varExist = true; }
       }
	}

	void ReadUserFileNames()
	{
	    int lCnt = 0;
	    //int lCnt2 = 0;
	    int Select = 0;

	    string strFile = "";
	    string lAns = "";
	    string str1 = "";

	    bool ck1 = false;

	    if(dta.size() > 0){ dta.clear(); }
        dta.resize(2, " ");

        while(varExist){
           ReadFile(4);

           if(varExist){
              WriteUserFile("");
              ReadFile(4);

              if(jsDataRaw.size() == 0){
                 while(!ck1){
                    titleScreen(4);
                    getline(cin, strFile);

                    cout<<"You entered [ "<<strFile<<" ]"<<endl;
                    cout<<"Is that correct?\n";
                    cout<<"Enter [y] or [n] => ";

                    getline(cin, lAns);
                    if((lAns == "y") || (lAns == "Y")){
                       ck1 = true;
                    }
                 }

                 WriteUserFile(strFile);
                 ReadFile(4);
              }
           }
        }

        lCnt = jsDataRaw.size();
        if(lCnt > 0){

            if(txtFileVar.size() > 0){ txtFileVar.clear(); }

            for(int i=0; i<lCnt; i++){

               dta[0] = jsDataRaw[i];
               dta[1] = jsDataRaw[i+1];
               txtFileVar.push_back(dta);
               ++i;
            }

            if(dta.size() > 0){ dta.clear(); }
            dta.resize(3, " ");
            dta[0] = "JSON/";
            dta[2] = ".json";

            ck1 = false;
            while(!ck1){
               titleScreen(5);
               cout<<"Select the number of the file you want to load => ";
               getline(cin, lAns);

               str1 = lAns;
               istringstream (str1) >> Select;

               for(int i=0; i<lCnt/2; i++){
                   if(Select == i+1){
                      ck1 = true;
                      break;
                   }
               }
            }

            dta[1] = dta[0] + txtFileVar[Select-1][0] + dta[2];
            FileName = dta[1];

            dta[1] = dta[0] + txtFileVar[Select-1][1] + dta[2];
            FileBKup = dta[1];

            if(dta.size() > 0){ dta.clear(); }
            if(txtFileVar.size() > 0){ txtFileVar.clear(); }
        }
	}

	void ReadVariables()
	{

	    if(dta.size() > 0){ dta.clear(); }
        dta.resize(2, " ");

        ReadFile(3);

        if(txtFileVar.size() == 0){

           for(int i=0; i<8; i++){
              txtFileVar.push_back(dta);
           }
           if(jsDataRaw.size() == 0){

              WriteVariables(0);
              ReadFile(3);
              updateUserVars();

              for(unsigned int i=0; i<jsDataRaw.size(); i++){
                 txtFileVar[i][0] = jsDataRaw[i];
              }

              userTitlesInfo();
           }
           else{

              if(jsDataRaw.size() > 0){ jsDataRaw.clear(); }
              if(txtFileVar.size() > 0){ txtFileVar.clear(); }

              ReadFile(3);

              for(unsigned int i=0; i<jsDataRaw.size(); i++){
                 txtFileVar.push_back(dta);
                 txtFileVar[i][0] = jsDataRaw[i];
              }

              updateUserVars();
           }
        }


        if(dta.size() > 0){ dta.clear(); }
        if(jsDataRaw.size() > 0){ jsDataRaw.clear(); }
	}

	void userTitlesInfo()
	{
	    string lAns = "n";

	    cout<<"Do you want to set a user defined math variable?"<<endl;
        while(lAns != "y"){
           cout<<"enter y / n => ";
           getline(cin, lAns);

           if((lAns == "y") || (lAns == "Y")){
              cout<<"entered ["<<lAns<<"]"<<endl;
              if(lAns == "Y"){ lAns = "y"; }

              setUserMathVar();
           }
           else{
             lAns = "y";
             titleScreen(1);
           }
        }
	}

	void setUserMathVar()
	{
	    string lAns1 = "";
	    string lAns2 = "n";
	    string str1 = "";
	    int lAnsInt = 0;
	    int lCnt = 0;

        vector <bool> numCk;
        bool ck = false;

        titleScreen(3);
        while(lAns2 != "y"){

            if(numCk.size() > 0){ numCk.clear(); }

            cout<<"enter number you want to change => ";
            getline(cin, lAns1);
            lCnt = lAns1.size();
            numCk.resize(lCnt, false);

         /* making sure the user entered only
            numeric values */
            for(int i=0; i<lCnt; i++){
               str1 = lAns1[i];
               istringstream (str1) >> lAnsInt;

               for(int j=0; j<10; j++){
                  if(lAnsInt == j+1){
                     numCk[i] = true;
                     break;
                  }
               }
            }

            for(int i=0; i<lCnt; i++){
               ck = numCk[i];
               if(!ck){ break; }
            }

            if(ck){

              istringstream (lAns1) >> lAnsInt;

              cout<<"\nCurrent name is [ "<<
              txtFileVar[lAnsInt-1][0]<<" ]"<<endl;

              cout<<"enter new name => ";
              getline(cin, lAns1);
              txtFileVar[lAnsInt-1][0] = lAns1;

              WriteVariables(1);
              titleScreen(3);

              cout<<"\nChange another y / n => ";
              getline(cin, lAns1);

              if((lAns1 == "n") || (lAns1 == "N")){
                 lAns2 = "y";
                 titleScreen(1);
              }
            }// end if(ck)
        }//     end while(lAns2 != "y")
	}

	void restoreBk()
	{
        clearArrays();
        ReadFile(1);
        WriteRestore();
        if(jsDataRaw.size() > 0){ jsDataRaw.clear(); }
        Setup("", "", "", "");
	}

	void setDataPosition(string strFrom, int fElm)
	{
	 // Clearing old data
		if(AccessData.size() > 0){ AccessData.clear(); }
        if(dta.size() > 0){ dta.clear(); }


	 /* setting the actual position where
	    the data came from for later
	    use (change, add, sub) */

	    dta.push_back(strFrom);
	    sprintf(ConvNum, "%d", fElm);
	    dta.push_back(ConvNum);

	    if(passData != ""){ dta.push_back(passData); }
		else{ dta.push_back(""); }

	    AccessData.push_back(dta);
	    convPubIntVal();
	}

	void SetMathDefaults()
	{
	    mathInt1 = 0;
	    mathInt2 = 0;
	    mathInt3 = 0;
	    mathInt4 = 0;
	    mathInt5 = 0;
	    mathInt6 = 0;
	    mathInt7 = 0;
	    mathInt8 = 0;
	}

	void showAll()
	{
	   int lRow = jsDataMod.size();

	   BeforeWriteCheck();

	   for(int i=0; i<lRow; i++){
	      cout<<jsDataMod[i][0]<<endl;
	   }
	}

	bool SingleBracket()
	{
		bool ck = false;

		int Cnt2BrketEnd, fElm, lElm, lElm2, lCnt;
		Cnt2BrketEnd = fElm = lElm = lElm2 = lCnt = 0;

		vector<int> localElm;
		string str1 = "";
		string str2 = "";
		string str3 = "";
		string str4 = "";
		string strAsso = "";

	 // the user typed keyword that has the []
		str1 = qList[cnt2-2];

	 // the user typed keyword after the dot
		str3 = qList[cnt2-1];

		for(unsigned int i=0; i<passData.size(); i++){
		    strAsso = passData[i];

            if(i==0){
		      if(strAsso == "["){
                 passData.erase(0, 1);
                 passData.erase(passData.size()-1);
		         istringstream (passData) >> lElm2;
		         lElm2++;
		      }
		    }
		}

		if(bracketID.size() < 2){

			 for(unsigned int i=0; i<keys.size(); i++){

			 // keyword from the file
			    str2 = keys[i];

				if(str1 == str2){

				/* using the modified array to find the end of
				   a section like ("horror genre"). so the range
				   is between the begining until then end of
				   the that section */

				   for(unsigned int j=i; j<jsDataMod.size(); j++){
					  str4 = jsDataMod[j][0];

					  if(str4 == "}"){
						  Cnt2BrketEnd = j;
						  break;
					  }
					}


				 /* selecting the keyData that matches what
				    the user typed, and storing it in a
				    local array */
					for(int j=i; j<Cnt2BrketEnd; j++){
					  str4 = keys[j];

					  if(str3 == str4){
						 localElm.push_back(j);
						 ck = true;
					  }
					}
				}
				if(ck){ break; }

			 } // end for(unsigned int i=0; i<keys.size(); i++)

		  /* storing the keyData in class variables
		     to be modified/utilized later */

             lElm = bracketID[bracketID.size()-1];
			 elmCK1 = fElm = localElm[lElm];

			 vStr = keyData[fElm];
			 setDataPosition("keyData", fElm);

			 cout<<"\ncurrent value "<<vStr<<endl;

		}
		return ck;
	}

	void subKeyAryCheck()
	{
	   string str1 = "";

    /* this loop determines if the keyData has a
       sub array. If so, load the array data in the
       assoKeyAry[] for later use */

	   for(int i=0; i<row; i++){

		 tmp1="";
		 line = keyData[i];
		 tmp1 = line[0];
		 cnt = 0;

 	  // clearing out old elements if they exists.
		 if(dta.size() != 0){ dta.clear(); }

         if(tmp1 == "["){

            dta.push_back("[");
	        str1 = "";
	        ogEndCnt = line.size() - 1;
	        str1 = line[ogEndCnt];

         // setting up the final characters for the array
	        if(str1 == ","){
		       strEnding = "],";
		       ogEndCnt -= 1;
		    }
		    else{ strEnding = "]"; }

         /* this is not a string like "Joe Mac"
	        it is a interger. So remove the
	        spaces in the string */

            stringstream ss1(line);

            while(ss1 >>buf1){
			   dta.push_back(buf1);
			}

            dta.push_back(strEnding);

            cnt = dta.size();

            for(int j=1; j<cnt-1; j++){

               if(j==1){

                  str1 = dta[j];

               // remove the "[" from the beginning
                  str1.erase(0, 1);
                  if(str1.size() > 0){
                    str1.erase(str1.size()-1, 1);
                    dta[j] = str1;
                  }
               }
               else{

                  str1 = dta[j];

               // remove the "," from the beginning
                  str1.erase(str1.size()-1, 1);
                  dta[j] = str1;

               }
            }

            cnt = dta.size();
            myString = "";

         // building the data string to be stored
            for(int j=0; j<cnt; j++){

		       if(j == 0){
			     myString += dta[j];
		       }

		       else if((j > 0) && (j < cnt-2)){
		  	      myString += dta[j];
				  myString += ", ";
			   }
			   else if((j == cnt-2) && (j < cnt)){
				  myString += dta[j];
			   }
			   else { myString += dta[j]; }
		    }

		 // only store data if it's not returning "[]"
		    if((myString != "[]") && (myString != "[[]")){

		     // storing the data perminately
		        assoKeyAry.push_back(dta);

		     // restoring the data string for later use
                jsDataMod[i][1] = myString;

             // storing the actual element locations
                sprintf(ConvNum, "%d", i);
                jsDataMod[i][2] = ConvNum;
            }
          } // end if(tmp1 == "[")
		} //   end for(int i=0; i<row; i++)
	}

	void updateAssoAry()
	{
	    bool ck = false;

        string str1 = "";
        string str2 = "";
        string str3 = "";
        vector <string> strCloseBrkt;

        int lCnt = 0;
        int lCnt2 = 0;
        int lElm = 0;
        int lElm2= 0;
        int lElm3 = 0;
        vector <int> localElm;

        if(assoKeyAry.size() > 0){

        // founding out how many key arrays there are
		   lCnt = keyData.size();
		   for(int i=0; i<lCnt; i++){
		      str1 = keyData[i];
		      lCnt2 = str1.size();

		      str2 = str1[0];
		      if(str2 == "["){
		         localElm.push_back(i);
		      }
		   }

		   lCnt = localElm.size();
		   for(int i=0; i<lCnt; i++){

		      str1 = keyData[localElm[i]];
		      str2 = jsDataMod[localElm[i] + 1][0];

		      if((str1 == "[") && (str2 == "{")){
		          localElm.erase(localElm.begin() + i);
		          i--;
		      }
		   }

		   lCnt = localElm.size();
		   strCloseBrkt.resize(lCnt, "");

		   for(int i=0; i<lCnt; i++){

              lElm = localElm[i];
              str1 = jsDataMod[lElm][1];
              str2 = keyData[lElm - 1];

              if((str2 == "{") && (i > 0)){

                 for(unsigned int j=lElm; j<jsDataMod.size(); j++){

                     str2 = jsDataMod[j][0];
                     if((str2 == "}") || (str2 == "},")){
                        lElm2 = j;
                        break;
                     }
                 }

                 lElm3 = i;

                 for(int j=lElm; j<lElm2-1; j++){
                    str2 = keyData[j];
                    str3 = str2[str2.size()-1];

                    if(str3 == "]"){
                      strCloseBrkt[lElm3] = ",";
                      lElm3++;
		            }
                 }

              }
		   }

        /* This section of code set the limit on
           on how writing shall be done from
           big files vs smaller files that have
           key associate arrays */

		   ck = false;
		   for(int i=0; i<lCnt; i++){
              str1 = strCloseBrkt[i];
              if(str1 != ""){
                ck = true;
                break;
              }
           }
           if(ck){ lCnt -= 1; }

        // building the key's array string
           for(int i=0; i<lCnt; i++){

              lCnt2 = assoKeyAry[i].size();
              str1 = "";

		      for(int j=0; j<lCnt2; j++){

		         if((j==0) || (j == lCnt2-1)){
		            str1 += assoKeyAry[i][j];
		         }
		         else if(j == lCnt2-2){
		            str1 += assoKeyAry[i][j];
		         }
		         else{
		            str1 += assoKeyAry[i][j];
		            str1 += ", ";
		         }
		      }

		      if(strCloseBrkt[i] != ""){
		         str1 += strCloseBrkt[i];
		      }

		      keyData[localElm[i]] = str1;

		   } // end for(int i=0; i<lCnt; i++)
		} // end if(assoKeyAry.size() > 0)

		if(localElm.size() > 0){ localElm.clear(); }
		if(strCloseBrkt.size() > 0){ strCloseBrkt.clear(); }
	}

	void updateNoKeyData()
	{
		int lCnt = noKeyAry.size();
		int lCnt2 = 0;
		int lElm = 0;
		string str1, str2;
		str1 = str2 = "";

		if(lCnt>0){
			for(int i=0; i<lCnt; i++){

				lCnt2 = noKeyAry[i].size();
				for(int j=0; j<lCnt2; j++){

				   str1 = noKeyAry[i][lCnt2-1];

	            // converting strElm to integer
		           istringstream (str1) >> lElm;

                   str2 = "[";

                   for(int k=0; k<lCnt2-1; k++){

					   if((i == lCnt-1) && (k == lCnt2-2)){
						   str2 += noKeyAry[i][k];
						   str2 += "]";
					   }
					   else if(k == lCnt2-2){
						   str2 += noKeyAry[i][k];
						   str2 += "],";
					   }
					   else{
						   str2 += noKeyAry[i][k];
						   str2 += ", ";
					   }
				   }

				   break;

				} // end for(int j=0; j<lCnt2; j++)

				jsDataMod[lElm][0] = str2;
				keyData[lElm] = " ";

			}// end for(int i=0; i<lCnt; i++)
		} //    end if(lCnt>0)
	}

    void updateUserVars()
    {
        string str2 = "";

     /* Convert user's numbers to strings
        for later use in math equations */

        sprintf(ConvNum, "%d", temp1);
        str2 = ConvNum;
        txtFileVar[0][1] = ConvNum;

        sprintf(ConvNum, "%d", oMovies);
        str2 = ConvNum;
        txtFileVar[1][1] = ConvNum;

        sprintf(ConvNum, "%d", nMovies);
        str2 = ConvNum;
        txtFileVar[2][1] = ConvNum;

        sprintf(ConvNum, "%d", cBars);
        str2 = ConvNum;
        txtFileVar[3][1] = ConvNum;

        sprintf(ConvNum, "%d", gummies);
        txtFileVar[4][1] = ConvNum;

        sprintf(ConvNum, "%d", bCandy);
        txtFileVar[5][1] = ConvNum;

        sprintf(ConvNum, "%d", popCorn);
        str2 = ConvNum;
        txtFileVar[6][1] = ConvNum;

        sprintf(ConvNum, "%d", mathInt1);
        str2 = ConvNum;
        txtFileVar[7][1] = ConvNum;

        sprintf(ConvNum, "%d", mathInt2);
        str2 = ConvNum;
        txtFileVar[8][1] = ConvNum;

        sprintf(ConvNum, "%d", mathInt3);
        str2 = ConvNum;
        txtFileVar[9][1] = ConvNum;

        sprintf(ConvNum, "%d", mathInt4);
        str2 = ConvNum;
        txtFileVar[10][1] = ConvNum;

        sprintf(ConvNum, "%d", mathInt5);
        txtFileVar[11][1] = ConvNum;

        sprintf(ConvNum, "%d", mathInt6);
        txtFileVar[12][1] = ConvNum;

        sprintf(ConvNum, "%d", mathInt7);
        txtFileVar[13][1] = ConvNum;

        sprintf(ConvNum, "%d", mathInt8);
        txtFileVar[14][1] = ConvNum;
    }

   void varPreload(int v1, int v2, int v3, int v4, int v5)
   {
     // clearing out old data
        if(varPreload1.size() > 0){ varPreload1.clear(); }

     // preloading user data for later use
        if(v1 > 0){ varPreload1.push_back(v1); }
        if(v2 > 0){ varPreload1.push_back(v2); }
        if(v3 > 0){ varPreload1.push_back(v3); }
        if(v4 > 0){ varPreload1.push_back(v4); }
        if(v5 > 0){ varPreload1.push_back(v5); }
   }

   void WriteBkup(int selector)
   {
	   myfile.open(FileBKup, ios::out | ios::in | ios::trunc);

       if(myfile.is_open()){

           switch(selector){
                case 0:
                   for(unsigned int i=0; i<jsDataRaw.size(); i++){
                      myfile<<jsDataRaw[i]<<"\n";
                   }
                   break;
                case 1:
                   WriteModifiedBkup();
                   break;
                default: break;
           }

           CloseFile();
       }
       else {cout<<"can not write to json file"<<endl;}
	}

	void WriteModifiedBkup()
	{
	    int lCnt = jsDataMod.size();

        for(int i=0; i<lCnt; i++){

		   cnt = spaceCnt[i];

		// load the spaces first
		   for(int j=0; j<cnt; j++){
			  myfile<<" ";
		   }

           myfile<<jsDataMod[i][0]<<endl;
	    }
	}

	void WriteRestore()
	{
	   myfile.open(FileName, ios::out | ios::in | ios::trunc);

       if(myfile.is_open()){

          for(unsigned int i=0; i<jsDataRaw.size(); i++){
             myfile<<jsDataRaw[i]<<"\n";
          }
          CloseFile();
       }
       else {cout<<"can not write to json file"<<endl;}
	}

	void WriteFile()
	{
      /**************
       * Write data *
       **************/

       int lCnt = jsDataMod.size();
       string str1, str2;

       myfile.open(FileName, ios::out | ios::in | ios::trunc);

       if(myfile.is_open())
       {
           for(int i=0; i<lCnt; i++){

		       cnt = spaceCnt[i];

		    // load the spaces first
			   for(int j=0; j<cnt; j++){
				  myfile<<" ";
			   }

			   str1 = jsDataMod[i][0];

			   if(str1 != str2){
			      myfile<<jsDataMod[i][0]<<endl;
			   }

			   else if(str1.size() == 1){
			      myfile<<jsDataMod[i][0]<<endl;
			   }

               str2 = jsDataMod[i][0];
		   }

	       CloseFile();
        }
        else {cout<<"can not write to json file"<<endl;}
	}

	void WriteUserFile(string strFile)
	{
	   vector <string> bFileString = {"_BK", ""};

	   myfile.open(FileUserFiles, ios::out | ios::in | ios::app);

	   if(myfile.is_open()){

	      if(strFile != ""){
              bFileString[1] = strFile;
              myfile<<bFileString[1]<<"\n";

              bFileString[1] += bFileString[0];
              myfile<<bFileString[1]<<"\n";

              if(bFileString.size() > 0){ bFileString.clear(); }
	      }
       }
       else {cout<<"can not write to json file"<<endl;}

       CloseFile();
	}

	void WriteVariables(int selector)
	{
	   myfile.open(FileVariables, ios::out | ios::in | ios::trunc);

	   if(myfile.is_open()){
	      switch(selector){
	         case 0:
	           for(int i=0; i<8; i++){
	              if(i<7){ myfile<<"mathInt"<<i+1<<"\n"; }
	              else{ myfile<<"mathInt"<<i+1; }
	           }
	           break;
	         case 1:
               for(unsigned int i=0; i<txtFileVar.size(); i++){
                  myfile<<txtFileVar[i][0]<<"\n";
               }
               break;
            default: break;
          }
       }
       else {cout<<"can not write to json file"<<endl;}

       CloseFile();
	}

	public:

	int vInt;
	float vFlt;
	string vStr;

 // Constructor
    jsonParse()
    {}

 // Destructor
    ~jsonParse()
    {
       clearArrays();
	}

	void getTmpHoldData(vector<string> &tmpHold)
	{
		for(unsigned int i=0; i<keyData.size(); i++){
		   tmpHold.push_back(keyData[i]);
		}
	}

	void JsonQuery(string qStr, bool &exitStatus)
	{
		command = tmp1 = tmp2 = tmp3 = "";
		BracketFound = false;
		hasDot = false;

     // parsing the user's typed info
		parseData(qStr);

		tmp3 = qList[0];
		if((tmp3 == "find") || (tmp3 == "change")){

		 // removing the command words from the array
			qList.erase(qList.begin() + 0);
		}


     /**********************
      * Command executions *
      **********************/

        tmp1 = command;
        if(tmp1 == "append"){
           appendFileName();
        }

        else if(tmp1 == "find"){
           find(hasDot);
        }

        else if(tmp1 == "change"){
		   find(hasDot);
           change1(hasDot);
		}
		else if(tmp1 == "clear"){
		   clearScreen();
		}
		else if(tmp1 == "math"){
		   mathmatics();
        }
		else if(tmp1 == "modify"){
		   modify();
        }
        else if(tmp1 == "variables"){
           setUserMathVar();
        }
        else if(tmp1 == "loadfile"){
           loadFile();
        }
        else if(tmp1 == "showall"){
           showAll();
        }
        else if(tmp1 == "restore"){
           restoreBk();
        }
        else if(tmp1 == "quit"){
		   cout<<"quiting program... ";
		   exitStatus = true;
		}
        else if(tmp1 == "write"){
		   cout<<"Data written to file"<<endl;
		   updateNoKeyData();
		   updateAssoAry();
		   BeforeWriteCheck();
		   WriteFile();
	    }

	 // clearing out old data
	    if(qList.size() > 0){ qList.clear(); }
	    if(qList2.size() > 0){ qList2.clear(); }
	    if(bracketID.size() > 0){ bracketID.clear(); }
	}

    void Setup(string file1, string file2, string file3, string file4)
    {
        bool beginWrite = false;

        if(FileName == ""){
        // this happens on the first run
  		   FileName = file1;
		   FileBKup = file2;
		   FileVariables = file3;
		   FileUserFiles = file4;
		   beginWrite = true;
		   SetMathDefaults();
        }

        ReadUserFileNames();
        ReadVariables();
        titleScreen(1);
		ReadFile(0);

        row = jsDataRaw.size();
        col = 4;

    /* ========================
        redefining the vectors
       ========================
	                                          n    4
	    (row=length) Vector is now jsDataMod[row][col] */

		jsDataMod.resize(row, vector<string>(col));
		//temp1 = new string*[row];

		for(unsigned int i=0; i<jsDataMod.size(); i++){
			jsDataMod[i][1] = " ";
			jsDataMod[i][2] = " ";
			jsDataMod[i][3] = " ";
		}

        keys.resize(row, "");
        keyData.resize(row, "");
        KeyBrkt.resize(row, "");
        EndComma.resize(row, "");
        spaceCnt.resize(row, 0);

        colonsCnt.resize(row, vector<unsigned int>(2));
        for(int i=0; i<row; i++){
			colonsCnt[i][0] = 0;
			colonsCnt[i][1] = 0;
		}

     /* Counting the left spaces of each line
        and storing them in the spaceCnt array */

        for(int i=0; i<row; i++){

		   cnt = 0;
		   line = jsDataRaw[i];

           for(unsigned int j=0; j<line.size(); j++){
			   tmp1 = line[j];
			   if(tmp1 == " "){ cnt++; }
			   else { spaceCnt[i] = cnt; break; }
		   }
		}

     // Store the stripped modified string
		for(int i=0; i<row; i++){
           line = jsDataRaw[i];
           line2 = "";
           cnt = spaceCnt[i];

           for(unsigned int j=cnt; j<line.size(); j++){
              line2 += line[j];
              jsDataMod[i][0] = line2;
		   }
		}

     /* Counting the ":" of each line
        and storing them in the colonCnt array */

		for(int i=0; i<row; i++){
		   cnt = 0;
		   line = jsDataMod[i][0];

           for(unsigned int j=0; j<line.size(); j++){
			   tmp1 = line[j];

			   if(tmp1 != ":"){ cnt++; }
			   else{
			    // json raw file cnt
				   colonsCnt[i][0] = cnt;
			   }
		   }
		}

     /* Storing the "," and "[" of each
        line in the their own array */

		for(int i=0; i<row; i++){

		   line = jsDataMod[i][0];

           tmp1 = line[line.size() -1];
           if(tmp1 == ","){
              EndComma[i] = ",";
           }

           if(tmp1 == "["){
              KeyBrkt[i] = "[";
           }

           if(tmp1 == "{"){
              KeyBrkt[i] = "{";
           }
		}

		nonKeyCheck();
		assoKeyCheck();
		subKeyAryCheck();
        nonKeyNameCheck();

		if(beginWrite){ WriteBkup(0); }

     // releasing raw read data from memory
		if(jsDataRaw.size() > 0){ jsDataRaw.clear(); }
	}


    void titleScreen(int display)
    {
	   string title, str1, str2 , str3;
       title = str1 = str2 = str3 = "";

	   system("clear");

	   if(FileName == ""){
	      title += "*****************************************************\n";
	      title += "  JsonParser Program:  progrmr (Cornelius Hemphill)  \n";
	      title += "*****************************************************\n";
	   }
	   else{
	      str1 = FileName;
	      str1.erase(0, 5);
	      title += "*****************************************************\n";
	      title += "  JsonParser Program: FILE = [ ";
          title += str1;
          title += " ]\n";
	      title += "*****************************************************\n";
	   }
	   if(display == 0){
	     title += "\nProgram reads the json files of all the users\n";
	     title += "connected to the site and updates their data\n";
	   }
	   else if(display == 1){
	     title += "===================                   \n";
	     title += "  Query commands                      \n";
	     title += "===================                   \n";
	     title += "=> append                             \n";
	     title += "=> change                             \n";
	     title += "=> clear                              \n";
	     title += "=> find                               \n";
	     title += "=> loadfile                           \n";
	     title += "=> math                               \n";
	     title += "=> modify                             \n";
	     title += "=> quit                               \n";
	     title += "=> restore                            \n";
	     title += "=> showall                            \n";
	     title += "=> variables                          \n";
	     title += "=> write                            \n\n";
	     title += "Type what you are searching for       \n";
	     title += "example (find \"Carey Woods\" age)    \n";
	     title += "Remember, names must be entered with \"";
	     title += " around them.                      \n\n";
       }
       else if(display == 2){
		 title += "===================                       \n";
	     title += "  Math commands                           \n";
	     title += "===================                       \n";
         title += "=> mul                                    \n";
         title += "=> div                                    \n";
         title += "=> add                                    \n";
         title += "=> sub                                    \n";
         title += "=> quit                                 \n\n";
		 title += "Please enter your math equations with     \n";
		 title += "a space between the operator(s)           \n";
		 title += "examp: currency.USD += 400                \n";
		 title += "                                          \n";
		 title += "----------------------------              \n";
		 title += "Or, type quit and then type               \n";
		 title += "                                          \n";
		 title += "find currency.USD                         \n";
		 title += "math                                      \n";
		 title += "add 400                                   \n";
		 title += "----------------------------            \n\n";
	   }
	   else if(display == 3){
		 title += "\n";
		 title += "============================ \n";
		 title += "--------------------------   \n";
		 title += "| User defined variables |   \n";
		 title += "--------------------------   \n";

		 for(unsigned int i=0; i<txtFileVar.size(); i++){

            sprintf(ConvNum, "%d", i+1);
		    title += ConvNum;
		    title += ") ";
		    str1 = txtFileVar[i][0];
		    title += txtFileVar[i][0];
		    title += "                  \n";
		 }
		 title += "===========================  \n";
	   }
	   else if(display == 4){
	     title += "\n";
	     title += "Enter the filename without the .json extention\n\n";
	     title += "======================  \n";
	     title += "   Please remember!     \n";
	     title += "======================\n\n";
	     title += "Your filename maybe case sensitive\n\n";
	   }
	   else if(display == 5){
		 title += "\n";
		 title += "============================ \n";
		 title += "--------------------------   \n";
		 title += "| User defined filenames |   \n";
		 title += "--------------------------   \n";

		 for(unsigned int i=0; i<txtFileVar.size(); i++){

            sprintf(ConvNum, "%d", i+1);
		    title += ConvNum;
		    title += ") ";
		    str1 = txtFileVar[i][0];
		    title += txtFileVar[i][0];
		    title += "                  \n";
		 }
		 title += "===========================  \n";
	   }
	   else if(display == 6){
		 title += "\n";
		 title += "============================ \n";
		 title += "--------------------------   \n";
		 title += "| User defined filenames |   \n";
		 title += "--------------------------   \n";

		 for(unsigned int i=0; i<txtFileVar.size(); i++){
		    title += txtFileVar[i][0];
		    title += "                  \n";
		 }

	     title += "\n";
	     title += "Enter the filename without\nthe .json extention\n\n";
	     title += "======================  \n";
	     title += "   Please remember!     \n";
	     title += "======================\n\n";
	     title += "Your filename maybe\ncase sensitive\n";
		 title += "===========================  \n\n";
	   }
	   else if(display == 7){
	     title += "\n";
	     title += "===========================   \n";
	     title += "    Modification Setup        \n";
	     title += "===========================   \n";
/*
        -------------------------------------
         will add this feature in the future
        -------------------------------------
	     title += "1) Create a new section       \n";
*/
	     title += "2) Add to existing section    \n";
	     title += "=========================== \n\n";
	   }
	   else if(display == 8){
	     title += "\n";
	     title += "=========================================   \n";
	     title += "    [ " + line + " ] Modification           \n";
	     title += "=========================================   \n";
	     title += "                                            \n";
	     title += "Select where you want to append after,      \n";

	     if(cnt7 == 1){
	        title += "or insert into.                          \n";
	     }
	     title +="\n";

         for(unsigned int i=0; i<jsDataMod.size(); i++){
            str1 = jsDataMod[i][0];
            sprintf(ConvNum, "%d", i+1);
            str2 = ConvNum;
            str3 = EndComma[i];

            if(i==0){
               title += "\n" + str2 + ") " + str1 + str3 + "\n";
            }
            else{

               if(str1 == "{"){
                  title += "\n" + str2 + ") " + str1 + str3 + "\n";
               }
               else if((keys[i] != "") && (keys[i] != " ")){
                  title += "\n" + str2 + ") " + keys[i] + ": " +
                  keyData[i] + str3 + "\n";
               }
               else if(keys[i] == ""){

                  title += "\n" + str2 + ") " + str1;

                  if(str3 != ","){
                     title += str3 + "\n";
                  }
               }
               else if((str1 == "}") || (str1 == "},") ||
                       (str1 == "]") || (str1 == "],")){

                  title += "\n" + str2 + ") " + str1 + str3 + "\n";
               }
            }
         } // for(unsigned int i=0; i<jsDataMod.size(); i++)

	     title += "\n================================ \n\n";
	   }
	   else if(display == 9){
	     title += "\n";
	     title += "===========================   \n";
	     title += "   Options Modification       \n";
	     title += "===========================   \n";
	     title += "\nAdd                         \n";
	     title += "-------                       \n";
	     title += "1) Key / KeyData              \n";
	     title += "2) Key / AssoArray            \n";
	     title += "3) NoKeyArray                 \n";


	     if(cnt7 == 1){

	        title += "                              \n";
	        title += "Delete                        \n";
	        title += "--------                      \n";
	        title += "4) Key / KeyData              \n";
	        title += "5) Key / AssoArray            \n";
	        title += "6) NoKeyArray                 \n";
	     }

	     title += "                              \n";
	     title += "=========================== \n\n";
	   }
	   else if(display == 10){
	      title += "";
	   }
       else {
		 title += "\n";
		 title += "==========================\n";
		 title += "  Program shutting down   \n";
		 title += "   thanks for using       \n";
		 title += " til next time...Good-bye \n";
		 title += "==========================\n";
	   }

	   cout<<title<<endl;
   }

};


/*******************
 *  Main function  *
 *******************/
int main(int argc, char* argv[])
{
    //vector <string> mUserFileData;
	string localFile;
	string localFile2;
	string localFile3;
	string localFile4;
	string jsQuery;
	string str1;
	bool exitStatus = false;

	jsonParse json;
    localFile = localFile2 = "";
	localFile3 = "JSON/variables.txt";
	localFile4 = "JSON/userFileNames.txt";
	json.Setup(localFile, localFile2, localFile3, localFile4);

	while(!exitStatus){

/*     These are examples where you can change and
       display the json data in the class function
       exp: type => find "Amanda" "friends" */

       cout<<"Enter here => ";
       getline(cin, jsQuery);
       json.JsonQuery(jsQuery, exitStatus);
       cout<<"\n";
       jsQuery = "";

    // slight delay between reads
       sleep_for(milliseconds(15));

    }

 // program is closing. show exit message
	json.titleScreen(100);
	return 0;
}
